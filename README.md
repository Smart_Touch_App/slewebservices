# Sales Logic Express Stats Service #

The Sales Logic Express stats service (Web Service) is a STATS collection tool to collect statistics of data changes for a particular route device (Remote/ Client Device). 

It collects the data change count between the device (remote DB) and the consolidated DB based on the last download time on the remote database until the current time. The difference count is based on the last_modified column for each table and agreegates into application groups which is provided as a SOAP response.

### Version ###

* Version 1.0

### Documentation ###

* The complete document for this webservice (SLE - Stats Services - Ver 01.docx) is available in this repository's Wiki Page.

### Configuration ###

The below setup should be configured in web.config file

* The IP address of the MS SQL server in which the consolidated DB is installed is required in the Appsetting section for this web service to collect server availability stats

        e.g. <add key="IPAddress" value="192.168.1.17"/>

### Database configuration ###

 The below setup should be configured in web.config file

* The SLE stats service needs two seperate connection strings needed in <connectionStrings> section.  The login credintials and SQL Instance Name for the Consolidated database should be provided in the <connectionStrings> section.

1. First connection string is required for general purposes with the key name "SLEEntities".

2. Second connection string is required for gathering CPU Usage with the key name "SLEAdmin".

### Tools Required ###

* IIS

### Deployment instructions ###

* IPAddress and Database configurations need to be modifired for each environment.