﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using StatsModels;
using StatsDL;

namespace StatService
{
    /// <summary>
    /// StatsService provides information about server/ SQL server's status
    ///     returns server avaialbility status
    ///     returns server's CPU utilisation
    ///     returns number of concurrent connections on sql server
    ///     returns database's delta chagnes
    ///     returns database's schema chagnes
    /// </summary>
    public class StatService : IStatService
    {
        public static string SLEConnection = "SLEEntities";
        string _environment = "", _endpoint = "";

        public StatService()
        {
            _environment = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["Environment"]);
            _endpoint = Convert.ToString(OperationContext.Current.EndpointDispatcher.EndpointAddress);
        }

        #region Check Server is Available
        /// <summary>
        /// "isServerAvailable" contract returns the specified server is available. It doesn't need any parameter but the IP address of target server has to configured 
        /// in web.config file as the key name is "IPAddress" (e.g: <add key="IPAddress" value="192.168.1.17"/> in appsettings section)
        /// </summary>
        /// <returns>
        /// returns true/false based on the availability of the server
        ///     - PingServer : pings server (Server IP address configured in web.config file in key name of "IPAddress") if ping server returns false the method return false;
        ///     - CheckServerCPU : checks CPU once the PingServer returns true then it checks sql server, if the CPU is less than 90% it returns true else false;
        /// </returns>
        public bool isServerAvailable()
        {
            bool _available=false;
            if (PingServer())//pings server is available
            {
                _available= CheckServerCPU();
            }

            //Logging to database
            //string _contract = Convert.ToString(new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name);
            //LogLib.LogDetails objLogDetails = new LogLib.LogDetails(rq.DeviceDeatil.DeviceID, rq, rsp, _environment, this.GetType().Name, _endpoint, _contract);
            //bool resp = new LogLib.LogBL().LogResponse(objLogDetails);

            return _available;
        }
        #endregion

        #region Check System is ready to Sync
        /// <summary>
        /// "DoHealthCheck" contract returns the server is ready to sync if server is available/ CPU less than 90% and leaves option to the user 
        /// whether to sync now or sync later by providing delta stats.
        /// </summary>
        /// <param name="rq">
        /// has properties to response what are the stats the user wants
        ///     - IsReadyToSync         - set true/false request to report the user about the system is ready to sync.
        ///     - ShowSchemaChanges     - set true/false request to report the user about any schema changes to sync.
        ///     - ShowDeltaChanges      - set true/false request to report the user about any delta changes to sync.
        ///     - ShowSqlServerStatus   - set true/false request to report the user about sql server CPU.
        ///     - ShowConcurrentDevices - set true/false request to report the user about number of current connections on the server.
        ///     - ShowSqlServerQueue    - set true/false request to report the user about number of quries in queue (sql server).
        ///     - DeviceDeatil          - set DeviceInfo as as request whether the server to identify the requested device delta changes.
        ///             the DeviceID, ScriptVersion and atleast one entry RemoteSubscription (subscription name and last download date time) details are mandatory.
        /// </param>
        /// <returns></returns>
        public HealthResponse DoHealthCheck(HealthCheckRequest rq)
        {
            string connectionString = ConfigurationManager.ConnectionStrings[SLEConnection].ConnectionString;
            SqlServerDL sqls = new SqlServerDL(connectionString);
            HealthResponse rsp = new HealthResponse();
            if (sqls.CheckSQLAvailablity())
            {
                rsp.Status = new Status();
                rsp.Status.Code = 0;
                rsp.Status.Description = "Success";
                rsp.ServerStats = new List<ServerInfo>();
                
                if (rq.ShowSqlServerStatus)
                {
                    List<ServerInfo> sql = new SqlServerDL().GetCPUStats("get_cpu_stats");
                    rsp.ServerStats.Add(sql[0]);
                }
                if (rq.IsReadyToSync)
                {
                    if (isServerAvailable())
                        rsp.ReadyToSync = true;
                }
                else
                {
                    rsp.ReadyToSync = false;
                }

                if (rq.ShowDeltaChanges)
                {
                    rsp.DeltaChangeDetails = new SqlServerDL().GetDeltaRows(rq);
                    rsp.IsMandatorySync = rsp.DeltaChangeDetails.Any(item => item.IsMandatorySync == true);
                }
                if(!rsp.IsMandatorySync)
                    rsp.IsMandatorySync = new SqlServerDL(connectionString).CheckGlobalSync(rq.DeviceDeatil.DeviceID);
                
                if (rq.ShowSchemaChanges)
                {
                    rsp.SchemaChangeDetails = new SqlServerDL(connectionString).GetSchemaChanges();
                    if (rsp.SchemaChangeDetails.Count > 0)
                    {
                        rsp.didSchemaChange = true;
                        rsp.IsMandatorySync = true;
                    }
                    else
                    {
                        rsp.didSchemaChange = false;
                    }
                }

                if (rq.ShowSqlServerQueue)
                {
                    rsp.NumSqlQueryQueue = new SqlServerDL().GetSQLQueryStats("get_currently_running_queries");
                }

                if (rq.ShowTruncatedTables)
                {
                    rsp.TruncatedTables = new SqlServerDL().GetTruncatedTables(rq);
                    if (rsp.TruncatedTables.Count > 0) 
                    {
                        rsp.IsMandatorySync = true;
                    }
                }
            }
            else
            {
                rsp.Status = new Status();
                rsp.Status.Code = 101;
                rsp.Status.Description = "Failed to open SQL Server connection";
            }
            //bool resp = new SqlServerDL(connectionString).LogResponse(rq, rsp);

            //Logging to database
            string _contract = Convert.ToString(new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name);
            LogLib.LogDetails objLogDetails = new LogLib.LogDetails(rq.DeviceDeatil.DeviceID, rq, rsp, _environment, this.GetType().Name, _endpoint, _contract);
            bool resp = new LogLib.LogBL().LogResponse(objLogDetails);

            sqls.CloseConnection();
            
            return rsp;
        }
        #endregion

        #region Private Methods
        private bool PingServer()
        {
            // get the system values
            string serverIPAddress = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["IPAddress"]);
            SqlServerDL sqls = new SqlServerDL();
            bool isServerAvailable = sqls.CheckServerAvailablity(serverIPAddress);

            if (isServerAvailable)
            {
                return true;
            }
            return false;
        }

        private bool CheckServerCPU()
        {
            SqlServerDL sqls = new SqlServerDL();
            List<ServerInfo> ServerCPU = sqls.GetCPUStats("get_cpu_stats");

            if (ServerCPU != null)
            {
                if (ServerCPU[0].CpuUtilization < 90)
                    return true;
            }
            return false;
        }
        #endregion
    }
}
