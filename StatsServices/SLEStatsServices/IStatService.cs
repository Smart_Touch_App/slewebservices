﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using StatsModels;

namespace StatService
{
    /// <summary>
    /// Service to get statistics of delta changes in consolidated database 
    ///     (the delta changes made from Reference database, other remote database(from device-iAnywhere database) and the changes directly made in consolidated database)
    /// </summary>
    [ServiceContract]
    public interface IStatService
    {
        /// <summary>
        /// "isServerAvailable" contract returns the specified server is available. It doesn't need any parameter but the IP address of target server has to configured 
        /// in web.config file as the key name is "IPAddress" (e.g: <add key="IPAddress" value="192.168.1.17"/> in appsettings section)
        /// </summary>
        /// <returns>
        /// returns true/false based on the availablility of the server
        ///     - PingServer : pings server (Server IP address configured in web.config file in key key name of "IPAddress") if ping server returns false the method return false;
        ///     - CheckServerCPU : checks CPU once the PingServer returns true then it checks sql server, if the CPU is less than 90% it returns true else false;
        /// </returns>
        [OperationContract]
        bool isServerAvailable();

        /// <summary>
        /// "DoHealthCheck" contract returns the server is ready to sync if server is available/ CPU less than 90% and leaves option to the user 
        /// whether to sync now or sync later by providing delta stats.
        /// </summary>
        /// <param name="rq">
        /// has properties to response what are the stats the user wants
        ///     - IsReadyToSync         - set true/false request to report the user about the system is ready to sync.
        ///     - ShowSchemaChanges     - set true/false request to report the user about any schema changes to sync.
        ///     - ShowDeltaChanges      - set true/false request to report the user about any delta changes to sync.
        ///     - ShowSqlServerStatus   - set true/false request to report the user about sql server CPU.
        ///     - ShowConcurrentDevices - set true/false request to report the user about number of current connections on the server.
        ///     - ShowSqlServerQueue    - set true/false request to report the user about number of quries in queue (sql server).
        ///     - DeviceDeatil          - set DeviceInfo as as request whether the server to identify the requested device delta changes.
        ///             the DeviceID, ScriptVersion and atleast one entry RemoteSubscription (subscription name and last download date time) details are mandatory.
        /// </param>
        /// <returns></returns>
        [OperationContract]
        HealthResponse DoHealthCheck(HealthCheckRequest rq);
                
    }

}
