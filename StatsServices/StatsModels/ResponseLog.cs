﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace StatsModels
{
    public class ResponseLog
    {
        [DataMember(Name = "ObjectType", Order = 1)]
        public int ResponseLogID { get; set; }

        [DataMember(Name = "DeviceID", Order = 2)]
        public string DeviceID { get; set; }

        [DataMember(Name = "ResponseOn", Order = 3)]
        public DateTime ResponseOn { get; set; }

        [DataMember(Name = "Request", Order = 4)]
        public XmlObjectSerializer Request { get; set; }

        [DataMember(Name = "Response", Order = 5)]
        public XmlObjectSerializer Response { get; set; }
    }
}
