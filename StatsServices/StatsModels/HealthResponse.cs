﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace StatsModels
{
    [DataContract]
    public class HealthResponse
    {
        [DataMember(Name = "Status", Order = 0)]
        public Status Status { get; set; }

        [DataMember(Name = "ReadyToSync", Order = 1)]
        public bool ReadyToSync { get; set; }

        [DataMember(Name = "ServerStats", Order = 2)]
        public List<ServerInfo> ServerStats { get; set; }

        [DataMember(Name = "DeltaChangeDetails", Order = 3)]
        public List<DeltaInfo> DeltaChangeDetails { get; set; }

        [DataMember(Name = "didSchemaChange", Order = 4)]
        public bool didSchemaChange { get; set; }

        [DataMember(Name = "SchemaChangeDetails", Order = 5)]
        public List<ChangeDetail> SchemaChangeDetails { get; set; }

        [DataMember(Name = "NumSqlQueryQueue", Order = 6)]
        public int NumSqlQueryQueue { get; set; }

        [DataMember(Name = "TruncatedTables", Order = 7)]
        public List<TruncateTable> TruncatedTables { get; set; }

        [DataMember(Name = "IsMandatorySync", Order = 8)]
        public bool IsMandatorySync { get; set; }
    }
}
