﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace StatsModels
{
    public class Articles
    {
        public string RemoteId { get; set; }
        public string SchemaName { get; set; }
        public string TableName { get; set; }
        public DateTime LastUploadTime { get; set; }
        public DateTime LastDownloadTime { get; set; }
        public string PublicationName { get; set; }        					
    }

    public class TruncateTable
    {
        [DataMember(Name = "SchemaName", Order = 1)]
        public string SchemaName { get; set; }
        [DataMember(Name = "TableName", Order = 2)]
        public string TableName { get; set; }
    }
}
