﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace StatsModels
{
    public class Status
    {
        [DataMember(Name = "Code", Order = 1)]
        public int Code { get; set; }

        [DataMember(Name = "Description", Order = 2)]
        public string Description { get; set; }
    }
}
