﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using StatsModels;

namespace StatsModels
{
    [DataContract]
    public class HealthCheckRequest
    {
        [DataMember(Name = "IsReadyToSync", Order = 10)]
        public bool IsReadyToSync { get; set; }

        [DataMember(Name = "ShowSchemaChanges", Order = 20)]
        public bool ShowSchemaChanges { get; set; }
        [DataMember(Name = "ShowDeltaChanges", Order = 30)]
        public bool ShowDeltaChanges { get; set; }
        [DataMember(Name = "ShowSqlServerStatus", Order = 40)]
        public bool ShowSqlServerStatus { get; set; }
        [DataMember(Name = "ShowConcurrentDevices", Order = 50)]
        public bool ShowConcurrentDevices { get; set; }
        [DataMember(Name = "ShowSqlServerQueue", Order = 60)]
        public bool ShowSqlServerQueue { get; set; }
        [DataMember(Name = "ShowTruncatedTables", Order = 70)]
        public bool ShowTruncatedTables { get; set; }

        [DataMember(Name = "DeviceDeatil", Order = 80)]
        public DeviceInfo DeviceDeatil { get; set; }
    }
}
