﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace StatsModels
{
    public class DeltaInfo
    {
        [DataMember(Name = "ApplicationGroup", Order = 1)]
        public string ApplicationGroup { get; set; }

        [DataMember(Name = "DeltaRows", Order = 2)]
        public Int32 DeltaRows { get; set; }

        [DataMember(Name = "IsMandatorySync", Order = 4)]
        public bool IsMandatorySync { get; set; }
    }
    public class DeltaChanges
    {
        public string RouteID { get; set; }
        public string CTSchema { get; set; }
        public string CTTable { get; set; }
        public int CTDeltaRows { get; set; }
    }
}
