﻿
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace StatsModels
{
    public class ServerInfo
    {
        [DataMember(Name = "Server", Order = 1)]
        public string Server { get; set; }

        [DataMember(Name = "CpuUtilization", Order = 2)]
        public float CpuUtilization { get; set; }

        [DataMember(Name = "NumOfDBConnections", Order = 3)]
        public int NumOfDBConnections { get; set; }

        [DataMember(Name = "NumOfServerConnections", Order = 4)]
        public int NumOfServerConnections { get; set; }
    }
}