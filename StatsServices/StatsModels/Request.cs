﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace StatsModels
{
    [DataContract]
    public class Request
    {
        [DataMember(Name = "ShowServerAvailable", Order = 0)]
        public bool ShowServerAvailable { get; set; }
        [DataMember(Name = "ShowCpuHealth", Order = 1)]
        public bool ShowCpuHealth { get; set; }

        [DataMember(Name = "DeviceDeatil", Order = 2)]
        public DeviceInfo DeviceDeatil { get; set; }
    }
}
