﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace StatsModels
{
    [DataContract]
    public class Response
    {
        [DataMember(Name = "Status", Order = 0)]
        public Status Status { get; set; }

        [DataMember(Name = "ServerIsAvailable", Order = 1)]
        public bool ServerIsAvailable { get; set; }

        [DataMember(Name = "ServerStats", Order = 5)]
        public List<ServerInfo> ServerStats { get; set; }
    }
}
