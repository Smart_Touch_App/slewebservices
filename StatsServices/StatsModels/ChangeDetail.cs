﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace StatsModels
{
    public class ChangeDetail
    {
        [DataMember(Name = "ObjectType", Order = 1)]
        public string ObjectType { get; set; }

        [DataMember(Name = "EventType", Order = 2)]
        public string EventType { get; set; }

        [DataMember(Name = "ObjectName", Order = 3)]
        public string ObjectName { get; set; }

        [DataMember(Name = "ObjectGroup", Order = 4)]
        public string ObjectGroup { get; set; }
    }
}
