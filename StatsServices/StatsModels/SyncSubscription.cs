﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace StatsModels
{
    public class SyncSubscription
    {
        [DataMember(Name = "PublicationName", Order = 1)]
        public string PublicationName { get; set; }

        [DataMember(Name = "LastDownload", Order = 2)]
        public DateTime LastDownload { get; set; }

        [DataMember(Name = "LastUpload", Order = 3)]
        public DateTime LastUpload { get; set; }

    }
}