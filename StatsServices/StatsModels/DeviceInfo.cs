﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace StatsModels
{
    [DataContract]
    public class DeviceInfo
    {

        [DataMember(Name = "DeviceID", Order = 11)]
        public string DeviceID { get; set; }
        [DataMember(Name = "DeviceName", Order = 12)]
        public string DeviceName { get; set; }
        [DataMember(Name = "ScriptVersion", Order = 13)]
        public string ScriptVersion { get; set; }
        [DataMember(Name = "Latitude", Order = 14)]
        public decimal Latitude { get; set; }
        [DataMember(Name = "Longitude", Order = 15)]
        public decimal Longitude { get; set; }
        [DataMember(Name = "LastSchemaUpdate", Order = 16)]
        public DateTime LastSchemaUpdate { get; set; }
        [DataMember(Name = "RemoteSubscription", Order = 17)]
        public List<SyncSubscription> RemoteSubscription { get; set; }
    }
}
