﻿
/*
	This stored procedure needs admin rights to execute
*/
CREATE PROCEDURE [dbo].[get_currently_running_queries]
AS
BEGIN
	SELECT session_id, request_id, start_time, [status], USER_NAME(user_id) as usr, wait_time, 
	total_elapsed_time AS elapTime, [text]
	FROM sys.dm_exec_requests r 
		CROSS APPLY sys.dm_exec_sql_text(sql_handle) st
		WHERE session_id IN(SELECT spid FROM sys.sysprocesses WHERE DB_NAME(dbid)=DB_NAME() AND spid>50)
END

GO