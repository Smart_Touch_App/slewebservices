CREATE TABLE [dbo].[tblResponseLog](
	[ResponseLogID] [int] IDENTITY(1,1) NOT NULL,
	[DeviceID] [varchar](100) NULL,
	[ResponseOn] [datetime] CONSTRAINT [DF_tblResponseLog_ResponseOn] DEFAULT GETDATE(),
	[Request] [xml] NULL,
	[Response] [xml] NULL
) ON [PRIMARY]


GO


