﻿CREATE TABLE [dbo].[tblApplicationGroup](
	[ApplicationGroupID] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationGroup] [varchar](128) NULL,
	[SortOrder] [int] NULL,
	[IsMandatorySync] [bit] NULL,
	[last_modified] [datetime] CONSTRAINT [DF_tblApplicationGroup_LastModifed]  DEFAULT (GETDATE()),
 CONSTRAINT [PK_tblApplicationGroup] PRIMARY KEY CLUSTERED 
(
	[ApplicationGroupID] ASC
)
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[tblApplicationGroup] ON 

INSERT [dbo].[tblApplicationGroup] ([ApplicationGroupID], [ApplicationGroup], [SortOrder], [IsMandatorySync], [last_modified]) VALUES (1, N'BusinessUnits', 40, 0, CAST(0x0000A49F00B76AF8 AS DateTime))
INSERT [dbo].[tblApplicationGroup] ([ApplicationGroupID], [ApplicationGroup], [SortOrder], [IsMandatorySync], [last_modified]) VALUES (2, N'Customer', 50, 0, CAST(0x0000A49F00B76AF8 AS DateTime))
INSERT [dbo].[tblApplicationGroup] ([ApplicationGroupID], [ApplicationGroup], [SortOrder], [IsMandatorySync], [last_modified]) VALUES (3, N'PaymentTerms', 30, 1, CAST(0x0000A49F00B76AF8 AS DateTime))
INSERT [dbo].[tblApplicationGroup] ([ApplicationGroupID], [ApplicationGroup], [SortOrder], [IsMandatorySync], [last_modified]) VALUES (4, N'Pricing', 10, 1, CAST(0x0000A49F00B76AF8 AS DateTime))
INSERT [dbo].[tblApplicationGroup] ([ApplicationGroupID], [ApplicationGroup], [SortOrder], [IsMandatorySync], [last_modified]) VALUES (5, N'Product', 20, 0, CAST(0x0000A49F00B76AF8 AS DateTime))
INSERT [dbo].[tblApplicationGroup] ([ApplicationGroupID], [ApplicationGroup], [SortOrder], [IsMandatorySync], [last_modified]) VALUES (6, N'UserDefineCodes', 60, 0, CAST(0x0000A49F00B76AF8 AS DateTime))
SET IDENTITY_INSERT [dbo].[tblApplicationGroup] OFF
GO
