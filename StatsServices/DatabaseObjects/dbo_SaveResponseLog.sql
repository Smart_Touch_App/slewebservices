﻿

CREATE PROCEDURE [dbo].[SaveResponseLog]
(
	@DeviceID VARCHAR(100)='',
	@Request VARCHAR(MAX),
	@Response VARCHAR(MAX)
)
AS
BEGIN
	INSERT INTO [dbo].[tblResponseLog] ([DeviceID], [Request], [Response])
	VALUES(@DeviceID, CAST(@Request AS XML), CAST(@Response AS XML))
END

GO