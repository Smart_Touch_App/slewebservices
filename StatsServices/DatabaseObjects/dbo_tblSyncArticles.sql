﻿
CREATE TABLE [dbo].[tblSyncArticles](
	[SyncArticleID] [int] IDENTITY(1,1) NOT NULL,
	[PublicationName] [varchar](100) NULL,
	[SchemaName] [varchar](128) NULL,
	[TableName] [varchar](128) NULL,
	[ActiveYN] [bit] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedOn] [datetime] NULL,
	[EditedBy] [varchar](100) NULL,
	[EditedOn] [datetime] NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [PK_tblSyncArticles] PRIMARY KEY CLUSTERED 
(
	[SyncArticleID] ASC
)
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblSyncArticles] ADD  CONSTRAINT [DF_tblSyncArticles_ActiveYN]  DEFAULT ((1)) FOR [ActiveYN]
GO
ALTER TABLE [dbo].[tblSyncArticles] ADD  CONSTRAINT [DF_tblSyncArticles_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[tblSyncArticles] ADD  CONSTRAINT [DF_tblSyncArticles_EditedOn]  DEFAULT (getdate()) FOR [EditedOn]
GO
ALTER TABLE [dbo].[tblSyncArticles] ADD  DEFAULT (getdate()) FOR [last_modified]
GO

/*
	As of now we have added the below list of tables for publication. during deployment add/remote tables for that publication. It is mandatory for stats collection
*/
SET IDENTITY_INSERT [dbo].[tblSyncArticles] ON 

GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (1, N'pub_everything', N'BUSDTA', N'User_Role_Map', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (2, N'pub_everything', N'BUSDTA', N'user_master', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (3, N'pub_everything', N'BUSDTA', N'Route_User_Map', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (4, N'pub_everything', N'BUSDTA', N'Route_Device_Map', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (5, N'pub_everything', N'BUSDTA', N'F90CA086', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (6, N'pub_everything', N'BUSDTA', N'F90CA042', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (7, N'pub_everything', N'BUSDTA', N'F90CA003', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (8, N'pub_everything', N'BUSDTA', N'F56M0001', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (9, N'pub_everything', N'BUSDTA', N'F56M0000', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (10, N'pub_everything', N'BUSDTA', N'F42119', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (11, N'pub_everything', N'BUSDTA', N'F42019', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (12, N'pub_everything', N'BUSDTA', N'F4106', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (13, N'pub_everything', N'BUSDTA', N'F4102', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (14, N'pub_everything', N'BUSDTA', N'F4101', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (15, N'pub_everything', N'BUSDTA', N'F41002', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (16, N'pub_everything', N'BUSDTA', N'F40942', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (17, N'pub_everything', N'BUSDTA', N'F40941', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (18, N'pub_everything', N'BUSDTA', N'F4092', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (19, N'pub_everything', N'BUSDTA', N'F4076', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (20, N'pub_everything', N'BUSDTA', N'F4075', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (21, N'pub_everything', N'BUSDTA', N'F4072', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (22, N'pub_everything', N'BUSDTA', N'F4071', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (23, N'pub_everything', N'BUSDTA', N'F4070', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (24, N'pub_everything', N'BUSDTA', N'F4015', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (25, N'pub_everything', N'BUSDTA', N'F40073', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (26, N'pub_everything', N'BUSDTA', N'F03012', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (27, N'pub_everything', N'BUSDTA', N'F0150', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (28, N'pub_everything', N'BUSDTA', N'F0116', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (29, N'pub_everything', N'BUSDTA', N'F01151', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (30, N'pub_everything', N'BUSDTA', N'F0115', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (31, N'pub_everything', N'BUSDTA', N'F0101', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (32, N'pub_everything', N'BUSDTA', N'F0014', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (33, N'pub_everything', N'BUSDTA', N'F0006', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (34, N'pub_everything', N'BUSDTA', N'F0005', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (35, N'pub_everything', N'BUSDTA', N'F0004', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (36, N'pub_everything', N'BUSDTA', N'UDCKEYLIST', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (37, N'pub_everything', N'BUSDTA', N'ReasonCodeMaster', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (38, N'pub_everything', N'BUSDTA', N'M0111', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (39, N'pub_everything', N'BUSDTA', N'M080111', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (40, N'pub_everything', N'BUSDTA', N'Order_Detail', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (41, N'pub_everything', N'BUSDTA', N'Order_Header', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (42, N'pub_everything', N'BUSDTA', N'PickOrder_Exception', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (43, N'pub_everything', N'BUSDTA', N'PickOrder', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (44, N'pub_everything', N'BUSDTA', N'Device_Master', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (45, N'pub_immediate', N'BUSDTA', N'Order_Detail', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (46, N'pub_immediate', N'BUSDTA', N'Order_Header', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (47, N'pub_immediate', N'BUSDTA', N'PickOrder', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (48, N'pub_immediate', N'BUSDTA', N'PickOrder_Exception', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (49, N'pub_validate_user', N'BUSDTA', N'User_Role_Map', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (50, N'pub_validate_user', N'BUSDTA', N'user_master', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (51, N'pub_validate_user', N'BUSDTA', N'Route_User_Map', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (52, N'pub_validate_user', N'BUSDTA', N'Route_Device_Map', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
INSERT [dbo].[tblSyncArticles] ([SyncArticleID], [PublicationName], [SchemaName], [TableName], [ActiveYN], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [last_modified]) VALUES (53, N'pub_validate_user', N'BUSDTA', N'Device_Master', 1, N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), N'admin', CAST(0x0000A4AC0093C8AC AS DateTime), CAST(0x0000A4C10137BDF5 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[tblSyncArticles] OFF
GO