﻿
CREATE TABLE [dbo].[tblGlobalSyncLog](
	[GlobalSyncLogID] [int] IDENTITY(1,1) NOT NULL,
	[GlobalSyncID] [int] NULL,
	[DeviceID] [varchar](100) NULL,
	[CreatedOn] [datetime] NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [PK_tblGlobalSyncLog] PRIMARY KEY CLUSTERED 
(
	[GlobalSyncLogID] ASC
)
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblGlobalSyncLog] ADD  CONSTRAINT [DF_tblGlobalSyncLog_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[tblGlobalSyncLog] ADD  DEFAULT (getdate()) FOR [last_modified]
GO

ALTER TABLE [dbo].[tblGlobalSyncLog]  WITH CHECK ADD  CONSTRAINT [FK_tblGlobalSyncLog_tblGlobalSync] FOREIGN KEY([GlobalSyncID])
REFERENCES [dbo].[tblGlobalSync] ([GlobalSyncID])
GO
ALTER TABLE [dbo].[tblGlobalSyncLog] CHECK CONSTRAINT [FK_tblGlobalSyncLog_tblGlobalSync]
GO
