﻿
CREATE PROCEDURE [dbo].[uspCheckGlobalSync] 
(
	@DeviceID VARCHAR(100)
)
AS
BEGIN
	SELECT Sync=CASE WHEN COUNT(1)>0 THEN 1 ELSE 0 END
	FROM tblGlobalSync AS gs
	WHERE GlobalSyncID NOT IN(SELECT GlobalSyncID FROM tblGlobalSyncLog AS gsl WHERE gsl.DeviceID=@DeviceID)
END

GO