﻿CREATE TABLE [dbo].[tblGlobalSync](
	[GlobalSyncID] [int] IDENTITY(1,1) NOT NULL,
	[VersionNum] [varchar](100) NULL,
	[CreatedOn] [datetime] NULL,
	[last_modified] [datetime] NOT NULL,
 CONSTRAINT [PK_tblGlobalSync] PRIMARY KEY CLUSTERED 
(
	[GlobalSyncID] ASC
)
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblGlobalSync] ADD  CONSTRAINT [DF_tblGlobalSync_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[tblGlobalSync] ADD  DEFAULT (getdate()) FOR [last_modified]
GO 