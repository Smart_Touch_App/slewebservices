﻿CREATE TABLE [dbo].[tblGroupTables](
	[GroupTableID] [int] IDENTITY(1,1) NOT NULL,
	[RefDBSchema] [varchar](128) NULL,
	[RefDBTable] [varchar](128) NULL,
	[ApplicationGroupID] [int] NULL,
	[ConDBTable] [varchar](128) NULL,
	[IsMandatorySync] [bit] NULL,
	[last_modified] [datetime] CONSTRAINT [df_tblGroupTables_lastModifed]  DEFAULT (getdate()),
 CONSTRAINT [PK_tblGroupTables] PRIMARY KEY CLUSTERED 
(
	[GroupTableID] ASC
)
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[tblGroupTables] ON 

INSERT [dbo].[tblGroupTables] ([GroupTableID], [RefDBSchema], [RefDBTable], [ApplicationGroupID], [ConDBTable], [IsMandatorySync], [last_modified]) VALUES (1, N'BUSDTA', N'F0004', 6, N'F0004', 0, CAST(0x0000A49F00C37288 AS DateTime))
INSERT [dbo].[tblGroupTables] ([GroupTableID], [RefDBSchema], [RefDBTable], [ApplicationGroupID], [ConDBTable], [IsMandatorySync], [last_modified]) VALUES (2, N'BUSDTA', N'F0005', 6, N'F0005', 0, CAST(0x0000A49F00C37288 AS DateTime))
INSERT [dbo].[tblGroupTables] ([GroupTableID], [RefDBSchema], [RefDBTable], [ApplicationGroupID], [ConDBTable], [IsMandatorySync], [last_modified]) VALUES (3, N'BUSDTA', N'F0006', 1, N'F0006', 0, CAST(0x0000A49F00C37288 AS DateTime))
INSERT [dbo].[tblGroupTables] ([GroupTableID], [RefDBSchema], [RefDBTable], [ApplicationGroupID], [ConDBTable], [IsMandatorySync], [last_modified]) VALUES (4, N'BUSDTA', N'F0014', 3, N'F0014', 0, CAST(0x0000A49F00C37288 AS DateTime))
INSERT [dbo].[tblGroupTables] ([GroupTableID], [RefDBSchema], [RefDBTable], [ApplicationGroupID], [ConDBTable], [IsMandatorySync], [last_modified]) VALUES (5, N'BUSDTA', N'F0101', 2, N'F0101', 1, CAST(0x0000A49F00C37288 AS DateTime))
INSERT [dbo].[tblGroupTables] ([GroupTableID], [RefDBSchema], [RefDBTable], [ApplicationGroupID], [ConDBTable], [IsMandatorySync], [last_modified]) VALUES (6, N'BUSDTA', N'F0101', 2, N'M0140', 0, CAST(0x0000A49F00C37288 AS DateTime))
INSERT [dbo].[tblGroupTables] ([GroupTableID], [RefDBSchema], [RefDBTable], [ApplicationGroupID], [ConDBTable], [IsMandatorySync], [last_modified]) VALUES (8, N'BUSDTA', N'F0115', 2, N'M0111', 0, CAST(0x0000A49F00C37288 AS DateTime))
INSERT [dbo].[tblGroupTables] ([GroupTableID], [RefDBSchema], [RefDBTable], [ApplicationGroupID], [ConDBTable], [IsMandatorySync], [last_modified]) VALUES (9, N'BUSDTA', N'F01151', 2, N'M0111', 0, CAST(0x0000A49F00C37288 AS DateTime))
INSERT [dbo].[tblGroupTables] ([GroupTableID], [RefDBSchema], [RefDBTable], [ApplicationGroupID], [ConDBTable], [IsMandatorySync], [last_modified]) VALUES (10, N'BUSDTA', N'F0116', 2, N'F0116', 0, CAST(0x0000A49F00C37288 AS DateTime))
INSERT [dbo].[tblGroupTables] ([GroupTableID], [RefDBSchema], [RefDBTable], [ApplicationGroupID], [ConDBTable], [IsMandatorySync], [last_modified]) VALUES (11, N'BUSDTA', N'F0150', 2, N'F0150', 0, CAST(0x0000A49F00C37288 AS DateTime))
INSERT [dbo].[tblGroupTables] ([GroupTableID], [RefDBSchema], [RefDBTable], [ApplicationGroupID], [ConDBTable], [IsMandatorySync], [last_modified]) VALUES (12, N'BUSDTA', N'F0150', 2, N'M0140', 0, CAST(0x0000A49F00C37288 AS DateTime))
INSERT [dbo].[tblGroupTables] ([GroupTableID], [RefDBSchema], [RefDBTable], [ApplicationGroupID], [ConDBTable], [IsMandatorySync], [last_modified]) VALUES (13, N'BUSDTA', N'F03012', 2, N'F03012', 0, CAST(0x0000A49F00C37288 AS DateTime))
INSERT [dbo].[tblGroupTables] ([GroupTableID], [RefDBSchema], [RefDBTable], [ApplicationGroupID], [ConDBTable], [IsMandatorySync], [last_modified]) VALUES (14, N'BUSDTA', N'F40073', 4, N'F40073', 0, CAST(0x0000A49F00C37288 AS DateTime))
INSERT [dbo].[tblGroupTables] ([GroupTableID], [RefDBSchema], [RefDBTable], [ApplicationGroupID], [ConDBTable], [IsMandatorySync], [last_modified]) VALUES (15, N'BUSDTA', N'F4070', 4, N'F4070', 0, CAST(0x0000A49F00C37288 AS DateTime))
INSERT [dbo].[tblGroupTables] ([GroupTableID], [RefDBSchema], [RefDBTable], [ApplicationGroupID], [ConDBTable], [IsMandatorySync], [last_modified]) VALUES (16, N'BUSDTA', N'F4071', 4, N'F4071', 0, CAST(0x0000A49F00C37288 AS DateTime))
INSERT [dbo].[tblGroupTables] ([GroupTableID], [RefDBSchema], [RefDBTable], [ApplicationGroupID], [ConDBTable], [IsMandatorySync], [last_modified]) VALUES (17, N'BUSDTA', N'F4072', 4, N'F4072', 0, CAST(0x0000A49F00C37288 AS DateTime))
INSERT [dbo].[tblGroupTables] ([GroupTableID], [RefDBSchema], [RefDBTable], [ApplicationGroupID], [ConDBTable], [IsMandatorySync], [last_modified]) VALUES (18, N'BUSDTA', N'F4075', 4, N'F4075', 0, CAST(0x0000A49F00C37288 AS DateTime))
INSERT [dbo].[tblGroupTables] ([GroupTableID], [RefDBSchema], [RefDBTable], [ApplicationGroupID], [ConDBTable], [IsMandatorySync], [last_modified]) VALUES (19, N'BUSDTA', N'F4076', 4, N'F4076', 0, CAST(0x0000A49F00C37288 AS DateTime))
INSERT [dbo].[tblGroupTables] ([GroupTableID], [RefDBSchema], [RefDBTable], [ApplicationGroupID], [ConDBTable], [IsMandatorySync], [last_modified]) VALUES (20, N'BUSDTA', N'F4092', 4, N'F4092', 0, CAST(0x0000A49F00C37288 AS DateTime))
INSERT [dbo].[tblGroupTables] ([GroupTableID], [RefDBSchema], [RefDBTable], [ApplicationGroupID], [ConDBTable], [IsMandatorySync], [last_modified]) VALUES (21, N'BUSDTA', N'F40941', 4, N'F40941', 0, CAST(0x0000A49F00C37288 AS DateTime))
INSERT [dbo].[tblGroupTables] ([GroupTableID], [RefDBSchema], [RefDBTable], [ApplicationGroupID], [ConDBTable], [IsMandatorySync], [last_modified]) VALUES (22, N'BUSDTA', N'F40942', 4, N'F40942', 0, CAST(0x0000A49F00C37288 AS DateTime))
INSERT [dbo].[tblGroupTables] ([GroupTableID], [RefDBSchema], [RefDBTable], [ApplicationGroupID], [ConDBTable], [IsMandatorySync], [last_modified]) VALUES (23, N'BUSDTA', N'F41001', 5, N'F41001', 0, CAST(0x0000A49F00C37288 AS DateTime))
INSERT [dbo].[tblGroupTables] ([GroupTableID], [RefDBSchema], [RefDBTable], [ApplicationGroupID], [ConDBTable], [IsMandatorySync], [last_modified]) VALUES (24, N'BUSDTA', N'F41002', 5, N'F41002', 0, CAST(0x0000A49F00C37288 AS DateTime))
INSERT [dbo].[tblGroupTables] ([GroupTableID], [RefDBSchema], [RefDBTable], [ApplicationGroupID], [ConDBTable], [IsMandatorySync], [last_modified]) VALUES (25, N'BUSDTA', N'F4101', 5, N'F4101', 0, CAST(0x0000A49F00C37288 AS DateTime))
INSERT [dbo].[tblGroupTables] ([GroupTableID], [RefDBSchema], [RefDBTable], [ApplicationGroupID], [ConDBTable], [IsMandatorySync], [last_modified]) VALUES (26, N'BUSDTA', N'F4102', 5, N'F4102', 0, CAST(0x0000A49F00C37288 AS DateTime))
INSERT [dbo].[tblGroupTables] ([GroupTableID], [RefDBSchema], [RefDBTable], [ApplicationGroupID], [ConDBTable], [IsMandatorySync], [last_modified]) VALUES (27, N'BUSDTA', N'F4105', 5, N'F4105', 0, CAST(0x0000A49F00C37288 AS DateTime))
INSERT [dbo].[tblGroupTables] ([GroupTableID], [RefDBSchema], [RefDBTable], [ApplicationGroupID], [ConDBTable], [IsMandatorySync], [last_modified]) VALUES (28, N'BUSDTA', N'F4106', 5, N'F4106', 0, CAST(0x0000A49F00C37288 AS DateTime))
INSERT [dbo].[tblGroupTables] ([GroupTableID], [RefDBSchema], [RefDBTable], [ApplicationGroupID], [ConDBTable], [IsMandatorySync], [last_modified]) VALUES (29, N'BUSDTA', N'F42140', 2, N'M0140', 0, CAST(0x0000A49F00C37288 AS DateTime))
SET IDENTITY_INSERT [dbo].[tblGroupTables] OFF

GO
ALTER TABLE [dbo].[tblGroupTables]  WITH CHECK ADD  CONSTRAINT [FK_tblGroupTables_tblApplicationGroup_ApplicationGroupID] FOREIGN KEY([ApplicationGroupID])
REFERENCES [dbo].[tblApplicationGroup] ([ApplicationGroupID])
GO
ALTER TABLE [dbo].[tblGroupTables] CHECK CONSTRAINT [FK_tblGroupTables_tblApplicationGroup_ApplicationGroupID]
GO
