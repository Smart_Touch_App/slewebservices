﻿CREATE TABLE [dbo].[tblSyncDetails](
	[ObjectID] [int] IDENTITY(1,1) NOT NULL,
	[Objectname] [varchar](50) NULL,
	[LastSyncVersion] [bigint] NULL,
	[TruncTimeAtCDB] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ObjectID] ASC
)
) ON [PRIMARY]

GO
