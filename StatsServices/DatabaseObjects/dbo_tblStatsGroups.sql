﻿CREATE TABLE [dbo].[tblStatsGroups](
	[StatsGroupID] [int] IDENTITY(1,1) NOT NULL,
	[GroupName] [varchar](128) NULL,
	[last_modified] [datetime] CONSTRAINT [DF_tblStatsGroups_lastModifed] DEFAULT (getdate()),
 CONSTRAINT [PK_tblStatsGroup] PRIMARY KEY CLUSTERED 
(
	[StatsGroupID] ASC
)
) ON [PRIMARY]

GO
