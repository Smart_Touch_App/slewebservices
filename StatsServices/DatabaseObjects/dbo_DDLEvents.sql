﻿CREATE TABLE [dbo].[DDLEvents](
	[EvntID] [int] IDENTITY(1,1) NOT NULL,
	[EventDate] [datetime] CONSTRAINT [DF_DDLEvents_EventDate]  DEFAULT (GETDATE()),
	[EventType] [nvarchar](64) NULL,
	[ObjectType] [nvarchar](64) NULL,
	[EventDDL] [nvarchar](max) NULL,
	[EventXML] [xml] NULL,
	[DatabaseName] [nvarchar](255) NULL,
	[SchemaName] [nvarchar](255) NULL,
	[ObjectName] [nvarchar](255) NULL,
	[HostName] [varchar](64) NULL,
	[IPAddress] [varchar](32) NULL,
	[ProgramName] [nvarchar](255) NULL,
	[LoginName] [nvarchar](255) NULL,
	[Status] [bit] CONSTRAINT [DF_DDLEvents_Status]  DEFAULT ((1))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO