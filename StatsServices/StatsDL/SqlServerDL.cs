﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Configuration;
using StatsModels;
using System.Linq;
using System.Data.Linq;
using MobileDataModel;

namespace StatsDL
{
    public class SqlServerDL
    {
        public static string SLEConnection = "SLEEntities";
        public static string SLEAdminConnection = "SLEAdmin";
        private SqlConnection conn = null;
        private SqlConnection connAdmin = null;
        private SqlDataReader reader = null;

        MobileDataModelDb db = new MobileDataModelDb();
        ~SqlServerDL()
        {
            if (conn != null)
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            if (connAdmin != null)
            {
                if (connAdmin.State == ConnectionState.Open)
                {
                    connAdmin.Close();
                }
            }
        }
        public SqlServerDL()
        {
            string connectionString = ConfigurationManager.ConnectionStrings[SLEConnection].ConnectionString;
            string SLEAdminConn = ConfigurationManager.ConnectionStrings[SLEAdminConnection].ConnectionString;
            connAdmin = new SqlConnection(SLEAdminConn);
        }

        public SqlServerDL(string connectionString)
        {
            conn = new SqlConnection(connectionString);
            conn.Open();
        }
        public void CloseConnection()
        {
            if (conn != null)
            {
                conn.Close(); conn.Dispose();
            }
            if (connAdmin != null)
            {
                connAdmin.Close(); connAdmin.Dispose();
            }
        }
        //gets stats of a server
        public List<ServerInfo> GetCPUStats(string spname)
        {
            List<ServerInfo> srvrs = new List<ServerInfo>();

            using (connAdmin)
            {
                try
                {
                    if (connAdmin.State == ConnectionState.Closed) connAdmin.Open();

                    SqlCommand cmd = new SqlCommand(spname, connAdmin);
                    cmd.CommandType = CommandType.StoredProcedure;

                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        // loop thru the result to get details
                        while (reader.Read())
                        {
                            ServerInfo sql = new ServerInfo();
                            sql.Server = "Sql Server";
                            sql.NumOfDBConnections = Convert.ToInt32(reader["db_connections_count"]);
                            sql.NumOfServerConnections = Convert.ToInt32(reader["db_connections_count"]) + Convert.ToInt32(reader["nondb_connections_count"]);
                            sql.CpuUtilization = float.Parse(Convert.ToString(reader["cpu_usage"]));
                            srvrs.Add(sql);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                finally
                {
                }
            }

            return srvrs;
        }

        public int GetSQLQueryStats(string spname)
        {
            Int32 noOfQuries = 0;
            using (connAdmin)
            {
                try
                {
                    if (connAdmin.State == ConnectionState.Closed) connAdmin.Open();

                    SqlCommand cmd = new SqlCommand(spname, connAdmin);
                    cmd.CommandType = CommandType.StoredProcedure;

                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        // loop thru the result to get details
                        while (reader.Read())
                        {
                            noOfQuries++;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                finally
                {
                }
            }

            return noOfQuries;
        }

        //depricated
        public List<DeltaInfo> GetDeltaSize(string DeviceID, string ScriptVersion)
        {
            List<DeltaInfo> deltaList = new List<DeltaInfo>();

            using (conn)
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("uspGetDeltaRows", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DeviceID", DbType.String).Value = DeviceID;
                    cmd.Parameters.Add("Script_Version", DbType.String).Value = ScriptVersion;
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        // loop thru the result to get details
                        while (reader.Read())
                        {
                            DeltaInfo di = new DeltaInfo();
                            di.ApplicationGroup = Convert.ToString(reader["ApplicationGroup"]);
                            di.DeltaRows = Convert.ToInt32(reader["DeltaRows"]);
                            di.IsMandatorySync = Convert.ToBoolean(reader["IsMandatorySync"]);
                            deltaList.Add(di);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return deltaList;
        }

        public class TableScript
        {
            public string TableName { get; set; }
            public string SchemaName { get; set; }
        }

        public List<DeltaInfo> GetDeltaRows(HealthCheckRequest hcr)
        {
            List<DeltaInfo> deltaRows = new List<DeltaInfo>();
            try
            {
                List<DeltaChanges> deltaList = new List<DeltaChanges>();

                // Retrives the table list for the script version 
                List<TableScript> objTblScript = (from mlscVer in db.MlScriptVersions
                                                  join mltbScr in db.MlTableScripts on mlscVer.VersionId equals mltbScr.VersionId
                                                  join mlT in db.MlTables on mltbScr.TableId equals mlT.TableId
                                                  join mlms in db.MlModelSchemas on mlT.Name equals mlms.TableName
                                                  where mlscVer.Name == hcr.DeviceDeatil.ScriptVersion
                                                  select new TableScript { TableName = mlT.Name, SchemaName = mlms.SchemaName }).Distinct().ToList();
                string remoteID = (from rdm in db.BUSDTA_RouteDeviceMap where rdm.DeviceId == hcr.DeviceDeatil.DeviceID select rdm.RemoteId).SingleOrDefault();

                // Gets list of tables and publication name and last download and upload time
                List<Articles> objArticles = (from mlpub in hcr.DeviceDeatil.RemoteSubscription //remote sub/pub name dwd & upd time
                                              from scv in objTblScript
                                              join sart in db.TblSyncArticles on new { scv.SchemaName, scv.TableName, mlpub.PublicationName } equals new { sart.SchemaName, sart.TableName, sart.PublicationName }
                                              select new Articles { RemoteId = remoteID, SchemaName = scv.SchemaName, TableName = scv.TableName, LastUploadTime = mlpub.LastUpload, LastDownloadTime = mlpub.LastDownload }).Distinct().ToList();

                // filter only latest download time for a table
                objArticles = (from a in objArticles
                               group a by new { a.SchemaName, a.TableName } into grp
                               select grp.OrderByDescending(x=>x.LastDownloadTime).FirstOrDefault()).ToList();

                // get route id for device
                var routeid = (from f in db.BUSDTA_F56M0001
                               join rdm in db.BUSDTA_RouteDeviceMap on f.Ffuser equals rdm.RouteId
                               where rdm.RemoteId == remoteID
                               select f.Ffrout).SingleOrDefault();
                MDMBase cont = new MDMBase();
                foreach (Articles va in objArticles)
                {
                    List<DeltaChanges> objDeltaChanges = new List<DeltaChanges>();
                    // check an8 (address book number) column to match with route (purpose to get number of records in a route)
                    var colAN8 = db.Database.SqlQuery<string>("SELECT TOP 1 REPLACE(COLUMN_NAME, 'AN8', '') AS Prefix FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='" + va.SchemaName + "' AND TABLE_NAME='" + va.TableName + "' AND COLUMN_NAME LIKE'%AN8'").SingleOrDefault();
                    if (colAN8 != null && colAN8.Count() > 0)
                    {
                        // gets count of records for the table since last sync group by different routes (using AC03 by joining aban8 )
                        objDeltaChanges = db.Database.SqlQuery<DeltaChanges>(@"SELECT MRID AS RouteID, '" + va.SchemaName + "' AS CTSchema, '" + va.TableName + "' AS CTTable,COUNT(DISTINCT " + va.TableName + "." + colAN8 + "AN8) AS CTDeltaRows "
                                                            + " FROM " + va.SchemaName + "." + va.TableName + " AS " + va.TableName
                                                            + " LEFT JOIN BUSDTA.M0140 AS M0140 ON " + va.TableName + "." + colAN8 + "AN8=M0140.RMAN8 LEFT JOIN BUSDTA.M0100 AS M0100 ON M0100.MRMCU=M0140.RMMCU "
                                                            + " WHERE M0140.RMYN='Y' AND CAST(MRID AS NUMERIC(8,0))="+ routeid +" AND " + va.TableName + ".last_modified>= '" + Convert.ToDateTime(va.LastDownloadTime).ToString("dd-MMM-yyyy hh:mm:ss tt") + "' GROUP BY MRID").ToList();
                        deltaList.AddRange(objDeltaChanges.Where(x => x.CTDeltaRows > 0));
                    }
                    else
                    {
                        // gets the count for the table (common for all route)
                        objDeltaChanges = db.Database.SqlQuery<DeltaChanges>(@"SELECT NULL AS RouteID, '" + va.SchemaName + "' AS CTSchema, '" + va.TableName + "' AS CTTable,COUNT(1) AS CTDeltaRows "
                                                            + "FROM " + va.SchemaName + "." + va.TableName + " AS " + va.TableName + " WHERE " + va.TableName + ".last_modified>= '" + Convert.ToDateTime(va.LastDownloadTime).ToString("dd-MMM-yyyy hh:mm:ss tt") + "'").ToList();
                        deltaList.AddRange(objDeltaChanges.Where(x => x.CTDeltaRows > 0));
                    }

                }
                var dr = (from dt in deltaList
                          select new
                          {
                              RouteId = dt.RouteID,
                              ApplicationGroup = "Other Group",
                              SchemaName=dt.CTSchema,
                              TableName=dt.CTTable,
                              DeltaRows = dt.CTDeltaRows,
                              IsMandatorySync = false
                          }).ToList();
                foreach (var d in dr)
                {
                    DeltaInfo delrow;
                    //get group name of this table
                    string applicationGroup = (from t in db.TblGroupTables where (t.RefDbSchema == d.SchemaName && t.ConDbTable == d.TableName) select t.TblApplicationGroup.ApplicationGroup).FirstOrDefault();
                    //get whether the group marked as mandatory
                    bool? isMandatorySyncGrp = (from t in db.TblGroupTables where (t.RefDbSchema == d.SchemaName && t.ConDbTable == d.TableName) select t.TblApplicationGroup.IsMandatorySync).FirstOrDefault();
                    //get whether the table marked as mandatory
                    bool? isMandatorySyncTbl = (from t in db.TblGroupTables where (t.RefDbSchema == d.SchemaName && t.ConDbTable == d.TableName) select t.IsMandatorySync).FirstOrDefault();
                    //if group name not available make it to other group
                    applicationGroup = applicationGroup == null ? "Other Group" : applicationGroup;

                    if (deltaRows.FindAll(x => x.ApplicationGroup == applicationGroup).Count > 0)
                    {                        
                        delrow = deltaRows.FindAll(x => x.ApplicationGroup == applicationGroup).FirstOrDefault();
                        delrow.DeltaRows += d.DeltaRows;
                        if (Convert.ToBoolean(isMandatorySyncGrp == true ? true : isMandatorySyncTbl == true ? true : false))
                            delrow.IsMandatorySync = Convert.ToBoolean(true);
                    }
                    else
                    {
                        delrow = new DeltaInfo();
                        delrow.ApplicationGroup = applicationGroup;
                        delrow.DeltaRows = d.DeltaRows;
                        delrow.IsMandatorySync = Convert.ToBoolean(isMandatorySyncGrp == true ? true : isMandatorySyncTbl == true ? true : false);
                        if (d.DeltaRows > 0)
                            deltaRows.Add(delrow);
                    }
                }
            }
            catch (Exception ex)
            {
                return deltaRows;
            }
            return deltaRows;
        }

        public bool CheckGlobalSync(string DeviceID)
        {
            bool gSync = false;

            using (conn)
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("uspCheckGlobalSync", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("DeviceID", DbType.String).Value = DeviceID;
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        // loop thru the result to get details
                        while (reader.Read())
                        {
                            gSync = Convert.ToBoolean(reader["Sync"]);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return gSync;
        }

        public List<ChangeDetail> GetSchemaChanges()
        {
            List<ChangeDetail> rslt = new List<ChangeDetail>();

            using (conn)
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SELECT d.ObjectType, d.EventType, d.ObjectName, g.GroupName AS ObjectGroup FROM dbo.DDLEvents AS d LEFT JOIN tblStatsGroup as g on d.ObjectName=g.TableName", conn);
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            ChangeDetail cd = new ChangeDetail();
                            cd.ObjectType = reader["ObjectType"].ToString();
                            cd.EventType = reader["EventType"].ToString();
                            cd.ObjectName = reader["ObjectName"].ToString();
                            cd.ObjectGroup = reader["ObjectGroup"].ToString();
                            rslt.Add(cd);
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }

            return rslt;
        }

        public List<TruncateTable> GetTruncatedTables(HealthCheckRequest hcr)
        {
            List<TruncateTable> TruncateTableList = new List<TruncateTable>();

            try
            {
                // Retrives the table list for the script version 
                List<TruncateTable> objTblScript = (from mlscVer in db.MlScriptVersions
                                                    join mltbScr in db.MlTableScripts on mlscVer.VersionId equals mltbScr.VersionId
                                                    join mlT in db.MlTables on mltbScr.TableId equals mlT.TableId
                                                    join sart in db.TblSyncArticles on mlT.Name equals sart.TableName
                                                    where mlscVer.Name == hcr.DeviceDeatil.ScriptVersion && sart.ActiveYn==true
                                                    select new TruncateTable { TableName = mlT.Name, SchemaName = sart.SchemaName }).Distinct().ToList();

                // Gets list of tables and publication name and last download and upload time
                TruncateTableList = (from mlpub in hcr.DeviceDeatil.RemoteSubscription //remote sub/pub name dwd & upd time
                                     from scv in objTblScript
                                     join sart in db.TblSyncArticles on new { scv.SchemaName, scv.TableName, mlpub.PublicationName } equals new { sart.SchemaName, sart.TableName, sart.PublicationName }
                                     join sdet in db.TblSyncDetails on (scv.SchemaName + "." + scv.TableName) equals sdet.Objectname
                                     where ((scv.SchemaName + "." + scv.TableName == sdet.Objectname) && sdet.TruncTimeAtCdb >= mlpub.LastDownload && sart.ActiveYn == true)
                                     select new TruncateTable { SchemaName = scv.SchemaName, TableName = scv.TableName }).GroupBy(x => x.TableName).Select(y => y.OrderBy(z => z.SchemaName).First()).ToList();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
            return TruncateTableList;
        }

        public bool CheckServerAvailablity(string serverIPAddress)
        {
            try
            {
                string _ipAddress = System.Configuration.ConfigurationManager.AppSettings["IPAddress"].ToString();
                var ping = new System.Net.NetworkInformation.Ping();
                System.Net.NetworkInformation.PingReply reply = ping.Send(_ipAddress, 60 * 1000);// 1 Sec time out (in ms)

                if (reply.Status == System.Net.NetworkInformation.IPStatus.Success)
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool CheckSQLAvailablity()
        {
            try
            {
                try
                {
                    if (conn.State == ConnectionState.Open)
                        return true;
                    else
                        return false;
                }
                catch (SqlException)
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        //public bool LogResponse(HealthCheckRequest request, HealthResponse resposne)
        //{
        //    string ReqXML = CreateXML(request);
        //    string RespXML = CreateXML(resposne);

        //    SqlCommand cmd = new SqlCommand("SaveResponseLog", conn);
        //    cmd.CommandType = CommandType.StoredProcedure;

        //    cmd.Parameters.Add("DeviceID", DbType.String).Value = request.DeviceDeatil == null ? "" : request.DeviceDeatil.DeviceID == null ? "" : request.DeviceDeatil.DeviceID;
        //    cmd.Parameters.Add("Request", DbType.String).Value = ReqXML;
        //    cmd.Parameters.Add("Response", DbType.String).Value = RespXML;
        //    using (conn)
        //    {
        //        cmd.ExecuteNonQuery();
        //    }
        //    return true;
        //}

        private string CreateXML(Object obj)
        {
            XmlDocument xmlDoc = new XmlDocument();   //Represents an XML document, 
            // Initializes a new instance of the XmlDocument class.          
            XmlSerializer xmlSerializer = new XmlSerializer(obj.GetType());
            // Creates a stream whose backing store is memory. 
            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializer.Serialize(xmlStream, obj);
                xmlStream.Position = 0;
                //Loads the XML document from the specified string.
                xmlDoc.Load(xmlStream);
                return xmlDoc.InnerXml;
            }
        }
    }
}
