﻿

// This file was automatically generated.
// Do not make changes directly to this file - edit the template instead.
// 
// The following connection settings were used to generate this file
// 
//     Configuration file:     "DBModels\App.config"
//     Connection String Name: "MobileDB"
//     Connection String:      "Data Source=HP-1\SQLEXPRESS2012;Initial Catalog=MobileDataModel_FPS4;User ID=DBA;Password=sql"

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier
// TargetFrameworkVersion = 4.5
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity.ModelConfiguration;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace MobileDataModel
{
    // ************************************************************************
    // Unit of work
    public interface IMDMBase : IDisposable
    {
        IDbSet<BUSDTA_F0101> BUSDTA_F0101 { get; set; }
        IDbSet<BUSDTA_F0101Del> BUSDTA_F0101Del { get; set; }
        IDbSet<BUSDTA_F56M0001> BUSDTA_F56M0001 { get; set; }
        IDbSet<BUSDTA_F56M0001Del> BUSDTA_F56M0001Del { get; set; }
        IDbSet<BUSDTA_RouteDeviceMap> BUSDTA_RouteDeviceMap { get; set; }
        IDbSet<BUSDTA_RouteDeviceMapDel> BUSDTA_RouteDeviceMapDel { get; set; }
        IDbSet<MlDatabase> MlDatabases { get; set; }
        IDbSet<MlModelSchema> MlModelSchemas { get; set; }
        IDbSet<MlScriptVersion> MlScriptVersions { get; set; }
        IDbSet<MlSubscription> MlSubscriptions { get; set; }
        IDbSet<MlTable> MlTables { get; set; }
        IDbSet<MlTableScript> MlTableScripts { get; set; }
        IDbSet<TblApplicationGroup> TblApplicationGroups { get; set; }
        IDbSet<TblGroupTable> TblGroupTables { get; set; }
        IDbSet<TblSyncArticle> TblSyncArticles { get; set; }
        IDbSet<TblSyncDetail> TblSyncDetails { get; set; }

        int SaveChanges();
        
        // Stored Procedures
        int SaveResponseLog(string deviceId, string request, string response);
        List<UspCheckGlobalSyncReturnModel> UspCheckGlobalSync(string deviceId, out int procResult);
    }

    // ************************************************************************
    // Database context
    [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
    public class MDMBase : DbContext, IMDMBase
    {
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public IDbSet<BUSDTA_F0101> BUSDTA_F0101 { get; set; }
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public IDbSet<BUSDTA_F0101Del> BUSDTA_F0101Del { get; set; }
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public IDbSet<BUSDTA_F56M0001> BUSDTA_F56M0001 { get; set; }
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public IDbSet<BUSDTA_F56M0001Del> BUSDTA_F56M0001Del { get; set; }
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public IDbSet<BUSDTA_RouteDeviceMap> BUSDTA_RouteDeviceMap { get; set; }
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public IDbSet<BUSDTA_RouteDeviceMapDel> BUSDTA_RouteDeviceMapDel { get; set; }
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public IDbSet<MlDatabase> MlDatabases { get; set; }
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public IDbSet<MlModelSchema> MlModelSchemas { get; set; }
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public IDbSet<MlScriptVersion> MlScriptVersions { get; set; }
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public IDbSet<MlSubscription> MlSubscriptions { get; set; }
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public IDbSet<MlTable> MlTables { get; set; }
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public IDbSet<MlTableScript> MlTableScripts { get; set; }
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public IDbSet<TblApplicationGroup> TblApplicationGroups { get; set; }
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public IDbSet<TblGroupTable> TblGroupTables { get; set; }
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public IDbSet<TblSyncArticle> TblSyncArticles { get; set; }
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public IDbSet<TblSyncDetail> TblSyncDetails { get; set; }
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        static MDMBase()
        {
            Database.SetInitializer<MDMBase>(null);
        }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public MDMBase()
            : base("Name=MobileDB")
        {
        }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public MDMBase(string connectionString) : base(connectionString)
        {
        }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public MDMBase(string connectionString, System.Data.Entity.Infrastructure.DbCompiledModel model) : base(connectionString, model)
        {
        }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new BUSDTA_F0101Configuration());
            modelBuilder.Configurations.Add(new BUSDTA_F0101DelConfiguration());
            modelBuilder.Configurations.Add(new BUSDTA_F56M0001Configuration());
            modelBuilder.Configurations.Add(new BUSDTA_F56M0001DelConfiguration());
            modelBuilder.Configurations.Add(new BUSDTA_RouteDeviceMapConfiguration());
            modelBuilder.Configurations.Add(new BUSDTA_RouteDeviceMapDelConfiguration());
            modelBuilder.Configurations.Add(new MlDatabaseConfiguration());
            modelBuilder.Configurations.Add(new MlModelSchemaConfiguration());
            modelBuilder.Configurations.Add(new MlScriptVersionConfiguration());
            modelBuilder.Configurations.Add(new MlSubscriptionConfiguration());
            modelBuilder.Configurations.Add(new MlTableConfiguration());
            modelBuilder.Configurations.Add(new MlTableScriptConfiguration());
            modelBuilder.Configurations.Add(new TblApplicationGroupConfiguration());
            modelBuilder.Configurations.Add(new TblGroupTableConfiguration());
            modelBuilder.Configurations.Add(new TblSyncArticleConfiguration());
            modelBuilder.Configurations.Add(new TblSyncDetailConfiguration());
        }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public static DbModelBuilder CreateModel(DbModelBuilder modelBuilder, string schema)
        {
            modelBuilder.Configurations.Add(new BUSDTA_F0101Configuration(schema));
            modelBuilder.Configurations.Add(new BUSDTA_F0101DelConfiguration(schema));
            modelBuilder.Configurations.Add(new BUSDTA_F56M0001Configuration(schema));
            modelBuilder.Configurations.Add(new BUSDTA_F56M0001DelConfiguration(schema));
            modelBuilder.Configurations.Add(new BUSDTA_RouteDeviceMapConfiguration(schema));
            modelBuilder.Configurations.Add(new BUSDTA_RouteDeviceMapDelConfiguration(schema));
            modelBuilder.Configurations.Add(new MlDatabaseConfiguration(schema));
            modelBuilder.Configurations.Add(new MlModelSchemaConfiguration(schema));
            modelBuilder.Configurations.Add(new MlScriptVersionConfiguration(schema));
            modelBuilder.Configurations.Add(new MlSubscriptionConfiguration(schema));
            modelBuilder.Configurations.Add(new MlTableConfiguration(schema));
            modelBuilder.Configurations.Add(new MlTableScriptConfiguration(schema));
            modelBuilder.Configurations.Add(new TblApplicationGroupConfiguration(schema));
            modelBuilder.Configurations.Add(new TblGroupTableConfiguration(schema));
            modelBuilder.Configurations.Add(new TblSyncArticleConfiguration(schema));
            modelBuilder.Configurations.Add(new TblSyncDetailConfiguration(schema));
            return modelBuilder;
        }
        
        // Stored Procedures
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int SaveResponseLog(string deviceId, string request, string response)
        {
            var deviceIdParam = new SqlParameter { ParameterName = "@DeviceID", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = deviceId, Size = 100 };
            var requestParam = new SqlParameter { ParameterName = "@Request", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = request };
            var responseParam = new SqlParameter { ParameterName = "@Response", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = response };
            var procResultParam = new SqlParameter { ParameterName = "@procResult", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output };
 
            Database.ExecuteSqlCommand("EXEC @procResult = SaveResponseLog @DeviceID, @Request, @Response", new object[]
            {
                deviceIdParam,
                requestParam,
                responseParam,
                procResultParam
 
            });
 
            return (int) procResultParam.Value;
        }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public List<UspCheckGlobalSyncReturnModel> UspCheckGlobalSync(string deviceId, out int procResult)
        {
            var deviceIdParam = new SqlParameter { ParameterName = "@DeviceID", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = deviceId, Size = 100 };
            var procResultParam = new SqlParameter { ParameterName = "@procResult", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output };
 
            var procResultData = Database.SqlQuery<UspCheckGlobalSyncReturnModel>("EXEC @procResult = uspCheckGlobalSync @DeviceID", new object[]
            {
                deviceIdParam,
                procResultParam

            }).ToList();
 
            procResult = (int) procResultParam.Value;
            return procResultData;
        }

    }

    // ************************************************************************
    // POCO classes

    [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
    public class BUSDTA_F0101
    {

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public decimal Aban8 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abalky { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abtax { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abalph { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abmcu { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Absic { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Ablngp { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abat1 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abcm { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abtaxc { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abat2 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public double? Aban81 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public double? Aban82 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public double? Aban83 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public double? Aban84 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public double? Aban86 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public double? Aban85 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abac01 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abac02 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abac03 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abac04 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abac05 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abac06 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abac07 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abac08 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abac09 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abac10 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abac11 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abac12 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abac13 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abac14 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abac15 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abac16 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abac17 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abac18 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abac19 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abac20 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abac21 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abac22 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abac23 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abac24 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abac25 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abac26 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abac27 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abac28 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abac29 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abac30 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abrmk { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abtxct { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abtx2 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Abalp1 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public DateTime LastModified { get; set; }
        
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public BUSDTA_F0101()
        {
            LastModified = System.DateTime.Now;
        }
    }

    [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
    public class BUSDTA_F0101Del
    {

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public decimal Aban8 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public DateTime? LastModified { get; set; }
        
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public BUSDTA_F0101Del()
        {
            LastModified = System.DateTime.Now;
        }
    }

    [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
    public class BUSDTA_F56M0001
    {

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Ffuser { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Ffrout { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Ffmcu { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Ffhmcu { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Ffbuval { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public double? Ffan8 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public double? Ffpa8 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Ffstop { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Ffzon { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Fflocn { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Fflocf { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Ffev01 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Ffev02 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Ffev03 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public double? Ffmath01 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public double? Ffmath02 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public double? Ffmath03 { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public decimal? Ffcxpj { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public decimal? Ffclrj { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public decimal? Ffdte { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public DateTime LastModified { get; set; }
        
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public BUSDTA_F56M0001()
        {
            LastModified = System.DateTime.Now;
        }
    }

    [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
    public class BUSDTA_F56M0001Del
    {

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Ffuser { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Ffrout { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Ffmcu { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public DateTime? LastModified { get; set; }
        
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public BUSDTA_F56M0001Del()
        {
            LastModified = System.DateTime.Now;
        }
    }

    [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
    public class BUSDTA_RouteDeviceMap
    {

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string RouteId { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string DeviceId { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int? Active { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string RemoteId { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public DateTime LastModified { get; set; }
        
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public BUSDTA_RouteDeviceMap()
        {
            LastModified = System.DateTime.Now;
        }
    }

    [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
    public class BUSDTA_RouteDeviceMapDel
    {

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string RouteId { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string DeviceId { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public DateTime? LastModified { get; set; }
        
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public BUSDTA_RouteDeviceMapDel()
        {
            LastModified = System.DateTime.Now;
        }
    }

    [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
    public class MlDatabase
    {

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int Rid { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string RemoteId { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public DateTime ScriptLdt { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public byte[] SeqId { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int SeqUploaded { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string SyncKey { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Description { get; set; }

        public virtual ICollection<MlSubscription> MlSubscriptions { get; set; }
        
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public MlDatabase()
        {
            SeqUploaded = 0;
            MlSubscriptions = new List<MlSubscription>();
        }
    }

    [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
    public class MlModelSchema
    {

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string SchemaType { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string SchemaName { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string TableName { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string ObjectName { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string DropStmt { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Checksum { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string DbChecksum { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public bool Locked { get; set; }
    }

    [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
    public class MlScriptVersion
    {

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int VersionId { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Name { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Description { get; set; }

        public virtual ICollection<MlTableScript> MlTableScripts { get; set; }
        
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public MlScriptVersion()
        {
            MlTableScripts = new List<MlTableScript>();
        }
    }

    [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
    public class MlSubscription
    {

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int Rid { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string SubscriptionId { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int UserId { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public decimal Progress { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string PublicationName { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public DateTime LastUploadTime { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public DateTime LastDownloadTime { get; set; }

        public virtual MlDatabase MlDatabase { get; set; }
        
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public MlSubscription()
        {
            SubscriptionId = "<unknown>";
            Progress = 0m;
            PublicationName = "<unknown>";
        }
    }

    [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
    public class MlTable
    {

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int TableId { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Name { get; set; }

        public virtual ICollection<MlTableScript> MlTableScripts { get; set; }
        
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public MlTable()
        {
            MlTableScripts = new List<MlTableScript>();
        }
    }

    [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
    public class MlTableScript
    {

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int VersionId { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int TableId { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Event { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int ScriptId { get; set; }

        public virtual MlScriptVersion MlScriptVersion { get; set; }
        public virtual MlTable MlTable { get; set; }
    }

    [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
    public class TblApplicationGroup
    {

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int ApplicationGroupId { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string ApplicationGroup { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int? SortOrder { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public bool? IsMandatorySync { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public DateTime? LastModified { get; set; }

        public virtual ICollection<TblGroupTable> TblGroupTables { get; set; }
        
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public TblApplicationGroup()
        {
            TblGroupTables = new List<TblGroupTable>();
        }
    }

    [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
    public class TblGroupTable
    {

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int GroupTableId { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string RefDbSchema { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string RefDbTable { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int? ApplicationGroupId { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string ConDbTable { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public bool? IsMandatorySync { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public DateTime? LastModified { get; set; }

        public virtual TblApplicationGroup TblApplicationGroup { get; set; }
    }

    [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
    public class TblSyncArticle
    {

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int SyncArticleId { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string PublicationName { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string SchemaName { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string TableName { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public bool? ActiveYn { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int? CreatedBy { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public DateTime? CreatedDateTime { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int? EditedBy { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public DateTime? EditedDateTime { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public DateTime LastModified { get; set; }
    }

    [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
    public class TblSyncDetail
    {

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int ObjectId { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Objectname { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public long? LastSyncVersion { get; set; }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public DateTime? TruncTimeAtCdb { get; set; }
    }


    // ************************************************************************
    // POCO Configuration

    internal class BUSDTA_F0101Configuration : EntityTypeConfiguration<BUSDTA_F0101>
    {
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public BUSDTA_F0101Configuration(string schema = "BUSDTA")
        {
            ToTable(schema + ".F0101");
            HasKey(x => x.Aban8);

            Property(x => x.Aban8).HasColumnName("ABAN8").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Abalky).HasColumnName("ABALKY").IsOptional().IsFixedLength().HasMaxLength(20);
            Property(x => x.Abtax).HasColumnName("ABTAX").IsOptional().IsFixedLength().HasMaxLength(20);
            Property(x => x.Abalph).HasColumnName("ABALPH").IsOptional().IsFixedLength().HasMaxLength(40);
            Property(x => x.Abmcu).HasColumnName("ABMCU").IsOptional().IsFixedLength().HasMaxLength(12);
            Property(x => x.Absic).HasColumnName("ABSIC").IsOptional().IsFixedLength().HasMaxLength(10);
            Property(x => x.Ablngp).HasColumnName("ABLNGP").IsOptional().IsFixedLength().HasMaxLength(2);
            Property(x => x.Abat1).HasColumnName("ABAT1").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abcm).HasColumnName("ABCM").IsOptional().IsFixedLength().HasMaxLength(2);
            Property(x => x.Abtaxc).HasColumnName("ABTAXC").IsOptional().IsFixedLength().HasMaxLength(1);
            Property(x => x.Abat2).HasColumnName("ABAT2").IsOptional().IsFixedLength().HasMaxLength(1);
            Property(x => x.Aban81).HasColumnName("ABAN81").IsOptional();
            Property(x => x.Aban82).HasColumnName("ABAN82").IsOptional();
            Property(x => x.Aban83).HasColumnName("ABAN83").IsOptional();
            Property(x => x.Aban84).HasColumnName("ABAN84").IsOptional();
            Property(x => x.Aban86).HasColumnName("ABAN86").IsOptional();
            Property(x => x.Aban85).HasColumnName("ABAN85").IsOptional();
            Property(x => x.Abac01).HasColumnName("ABAC01").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abac02).HasColumnName("ABAC02").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abac03).HasColumnName("ABAC03").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abac04).HasColumnName("ABAC04").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abac05).HasColumnName("ABAC05").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abac06).HasColumnName("ABAC06").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abac07).HasColumnName("ABAC07").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abac08).HasColumnName("ABAC08").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abac09).HasColumnName("ABAC09").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abac10).HasColumnName("ABAC10").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abac11).HasColumnName("ABAC11").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abac12).HasColumnName("ABAC12").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abac13).HasColumnName("ABAC13").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abac14).HasColumnName("ABAC14").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abac15).HasColumnName("ABAC15").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abac16).HasColumnName("ABAC16").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abac17).HasColumnName("ABAC17").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abac18).HasColumnName("ABAC18").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abac19).HasColumnName("ABAC19").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abac20).HasColumnName("ABAC20").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abac21).HasColumnName("ABAC21").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abac22).HasColumnName("ABAC22").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abac23).HasColumnName("ABAC23").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abac24).HasColumnName("ABAC24").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abac25).HasColumnName("ABAC25").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abac26).HasColumnName("ABAC26").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abac27).HasColumnName("ABAC27").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abac28).HasColumnName("ABAC28").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abac29).HasColumnName("ABAC29").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abac30).HasColumnName("ABAC30").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Abrmk).HasColumnName("ABRMK").IsOptional().IsFixedLength().HasMaxLength(30);
            Property(x => x.Abtxct).HasColumnName("ABTXCT").IsOptional().IsFixedLength().HasMaxLength(20);
            Property(x => x.Abtx2).HasColumnName("ABTX2").IsOptional().IsFixedLength().HasMaxLength(20);
            Property(x => x.Abalp1).HasColumnName("ABALP1").IsOptional().IsFixedLength().HasMaxLength(40);
            Property(x => x.LastModified).HasColumnName("last_modified").IsRequired();
        }
    }

    internal class BUSDTA_F0101DelConfiguration : EntityTypeConfiguration<BUSDTA_F0101Del>
    {
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public BUSDTA_F0101DelConfiguration(string schema = "BUSDTA")
        {
            ToTable(schema + ".F0101_del");
            HasKey(x => x.Aban8);

            Property(x => x.Aban8).HasColumnName("ABAN8").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.LastModified).HasColumnName("last_modified").IsOptional();
        }
    }

    internal class BUSDTA_F56M0001Configuration : EntityTypeConfiguration<BUSDTA_F56M0001>
    {
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public BUSDTA_F56M0001Configuration(string schema = "BUSDTA")
        {
            ToTable(schema + ".F56M0001");
            HasKey(x => new { x.Ffuser, x.Ffrout, x.Ffmcu });

            Property(x => x.Ffuser).HasColumnName("FFUSER").IsRequired().IsFixedLength().HasMaxLength(10).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Ffrout).HasColumnName("FFROUT").IsRequired().IsFixedLength().HasMaxLength(3).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Ffmcu).HasColumnName("FFMCU").IsRequired().IsFixedLength().HasMaxLength(12).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Ffhmcu).HasColumnName("FFHMCU").IsOptional().IsFixedLength().HasMaxLength(12);
            Property(x => x.Ffbuval).HasColumnName("FFBUVAL").IsOptional().IsFixedLength().HasMaxLength(12);
            Property(x => x.Ffan8).HasColumnName("FFAN8").IsOptional();
            Property(x => x.Ffpa8).HasColumnName("FFPA8").IsOptional();
            Property(x => x.Ffstop).HasColumnName("FFSTOP").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Ffzon).HasColumnName("FFZON").IsOptional().IsFixedLength().HasMaxLength(3);
            Property(x => x.Fflocn).HasColumnName("FFLOCN").IsOptional().IsFixedLength().HasMaxLength(20);
            Property(x => x.Fflocf).HasColumnName("FFLOCF").IsOptional().IsFixedLength().HasMaxLength(20);
            Property(x => x.Ffev01).HasColumnName("FFEV01").IsOptional().IsFixedLength().HasMaxLength(1);
            Property(x => x.Ffev02).HasColumnName("FFEV02").IsOptional().IsFixedLength().HasMaxLength(1);
            Property(x => x.Ffev03).HasColumnName("FFEV03").IsOptional().IsFixedLength().HasMaxLength(1);
            Property(x => x.Ffmath01).HasColumnName("FFMATH01").IsOptional();
            Property(x => x.Ffmath02).HasColumnName("FFMATH02").IsOptional();
            Property(x => x.Ffmath03).HasColumnName("FFMATH03").IsOptional();
            Property(x => x.Ffcxpj).HasColumnName("FFCXPJ").IsOptional();
            Property(x => x.Ffclrj).HasColumnName("FFCLRJ").IsOptional();
            Property(x => x.Ffdte).HasColumnName("FFDTE").IsOptional();
            Property(x => x.LastModified).HasColumnName("last_modified").IsRequired();
        }
    }

    internal class BUSDTA_F56M0001DelConfiguration : EntityTypeConfiguration<BUSDTA_F56M0001Del>
    {
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public BUSDTA_F56M0001DelConfiguration(string schema = "BUSDTA")
        {
            ToTable(schema + ".F56M0001_del");
            HasKey(x => new { x.Ffuser, x.Ffrout, x.Ffmcu });

            Property(x => x.Ffuser).HasColumnName("FFUSER").IsRequired().IsFixedLength().HasMaxLength(10).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Ffrout).HasColumnName("FFROUT").IsRequired().IsFixedLength().HasMaxLength(3).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Ffmcu).HasColumnName("FFMCU").IsRequired().IsFixedLength().HasMaxLength(12).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.LastModified).HasColumnName("last_modified").IsOptional();
        }
    }

    internal class BUSDTA_RouteDeviceMapConfiguration : EntityTypeConfiguration<BUSDTA_RouteDeviceMap>
    {
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public BUSDTA_RouteDeviceMapConfiguration(string schema = "BUSDTA")
        {
            ToTable(schema + ".Route_Device_Map");
            HasKey(x => new { x.RouteId, x.DeviceId });

            Property(x => x.RouteId).HasColumnName("Route_Id").IsRequired().IsUnicode(false).HasMaxLength(8).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.DeviceId).HasColumnName("Device_Id").IsRequired().IsUnicode(false).HasMaxLength(30).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Active).HasColumnName("Active").IsOptional();
            Property(x => x.RemoteId).HasColumnName("Remote_Id").IsOptional().IsUnicode(false).HasMaxLength(30);
            Property(x => x.LastModified).HasColumnName("last_modified").IsRequired();
        }
    }

    internal class BUSDTA_RouteDeviceMapDelConfiguration : EntityTypeConfiguration<BUSDTA_RouteDeviceMapDel>
    {
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public BUSDTA_RouteDeviceMapDelConfiguration(string schema = "BUSDTA")
        {
            ToTable(schema + ".Route_Device_Map_del");
            HasKey(x => new { x.RouteId, x.DeviceId });

            Property(x => x.RouteId).HasColumnName("Route_Id").IsRequired().IsUnicode(false).HasMaxLength(8).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.DeviceId).HasColumnName("Device_Id").IsRequired().IsUnicode(false).HasMaxLength(30).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.LastModified).HasColumnName("last_modified").IsOptional();
        }
    }

    internal class MlDatabaseConfiguration : EntityTypeConfiguration<MlDatabase>
    {
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public MlDatabaseConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".ml_database");
            HasKey(x => x.Rid);

            Property(x => x.Rid).HasColumnName("rid").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.RemoteId).HasColumnName("remote_id").IsRequired().IsUnicode(false).HasMaxLength(128);
            Property(x => x.ScriptLdt).HasColumnName("script_ldt").IsRequired();
            Property(x => x.SeqId).HasColumnName("seq_id").IsOptional().HasMaxLength(16);
            Property(x => x.SeqUploaded).HasColumnName("seq_uploaded").IsRequired();
            Property(x => x.SyncKey).HasColumnName("sync_key").IsOptional().IsUnicode(false).HasMaxLength(40);
            Property(x => x.Description).HasColumnName("description").IsOptional().IsUnicode(false).HasMaxLength(128);
        }
    }

    internal class MlModelSchemaConfiguration : EntityTypeConfiguration<MlModelSchema>
    {
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public MlModelSchemaConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".ml_model_schema");
            HasKey(x => new { x.SchemaType, x.SchemaName, x.TableName, x.ObjectName });

            Property(x => x.SchemaType).HasColumnName("schema_type").IsRequired().IsUnicode(false).HasMaxLength(32).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.SchemaName).HasColumnName("schema_name").IsRequired().IsUnicode(false).HasMaxLength(128).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.TableName).HasColumnName("table_name").IsRequired().IsUnicode(false).HasMaxLength(128).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.ObjectName).HasColumnName("object_name").IsRequired().IsUnicode(false).HasMaxLength(128).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.DropStmt).HasColumnName("drop_stmt").IsRequired().IsUnicode(false).HasMaxLength(2000);
            Property(x => x.Checksum).HasColumnName("checksum").IsRequired().IsUnicode(false).HasMaxLength(64);
            Property(x => x.DbChecksum).HasColumnName("db_checksum").IsOptional().IsUnicode(false).HasMaxLength(64);
            Property(x => x.Locked).HasColumnName("locked").IsRequired();
        }
    }

    internal class MlScriptVersionConfiguration : EntityTypeConfiguration<MlScriptVersion>
    {
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public MlScriptVersionConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".ml_script_version");
            HasKey(x => x.VersionId);

            Property(x => x.VersionId).HasColumnName("version_id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Name).HasColumnName("name").IsRequired().IsUnicode(false).HasMaxLength(128);
            Property(x => x.Description).HasColumnName("description").IsOptional();
        }
    }

    internal class MlSubscriptionConfiguration : EntityTypeConfiguration<MlSubscription>
    {
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public MlSubscriptionConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".ml_subscription");
            HasKey(x => new { x.Rid, x.SubscriptionId });

            Property(x => x.Rid).HasColumnName("rid").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.SubscriptionId).HasColumnName("subscription_id").IsRequired().IsUnicode(false).HasMaxLength(128).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.UserId).HasColumnName("user_id").IsRequired();
            Property(x => x.Progress).HasColumnName("progress").IsRequired();
            Property(x => x.PublicationName).HasColumnName("publication_name").IsRequired().IsUnicode(false).HasMaxLength(128);
            Property(x => x.LastUploadTime).HasColumnName("last_upload_time").IsRequired();
            Property(x => x.LastDownloadTime).HasColumnName("last_download_time").IsRequired();

            HasRequired(a => a.MlDatabase).WithMany(b => b.MlSubscriptions).HasForeignKey(c => c.Rid);
        }
    }

    internal class MlTableConfiguration : EntityTypeConfiguration<MlTable>
    {
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public MlTableConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".ml_table");
            HasKey(x => x.TableId);

            Property(x => x.TableId).HasColumnName("table_id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Name).HasColumnName("name").IsRequired().IsUnicode(false).HasMaxLength(128);
        }
    }

    internal class MlTableScriptConfiguration : EntityTypeConfiguration<MlTableScript>
    {
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public MlTableScriptConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".ml_table_script");
            HasKey(x => new { x.VersionId, x.TableId, x.Event });

            Property(x => x.VersionId).HasColumnName("version_id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.TableId).HasColumnName("table_id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Event).HasColumnName("event").IsRequired().IsUnicode(false).HasMaxLength(128).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.ScriptId).HasColumnName("script_id").IsRequired();

            HasRequired(a => a.MlScriptVersion).WithMany(b => b.MlTableScripts).HasForeignKey(c => c.VersionId);
            HasRequired(a => a.MlTable).WithMany(b => b.MlTableScripts).HasForeignKey(c => c.TableId);
        }
    }

    internal class TblApplicationGroupConfiguration : EntityTypeConfiguration<TblApplicationGroup>
    {
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public TblApplicationGroupConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".tblApplicationGroup");
            HasKey(x => x.ApplicationGroupId);

            Property(x => x.ApplicationGroupId).HasColumnName("ApplicationGroupID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.ApplicationGroup).HasColumnName("ApplicationGroup").IsOptional().IsUnicode(false).HasMaxLength(128);
            Property(x => x.SortOrder).HasColumnName("SortOrder").IsOptional();
            Property(x => x.IsMandatorySync).HasColumnName("IsMandatorySync").IsOptional();
            Property(x => x.LastModified).HasColumnName("last_modified").IsOptional();
        }
    }

    internal class TblGroupTableConfiguration : EntityTypeConfiguration<TblGroupTable>
    {
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public TblGroupTableConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".tblGroupTables");
            HasKey(x => x.GroupTableId);

            Property(x => x.GroupTableId).HasColumnName("GroupTableID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.RefDbSchema).HasColumnName("RefDBSchema").IsOptional().IsUnicode(false).HasMaxLength(128);
            Property(x => x.RefDbTable).HasColumnName("RefDBTable").IsOptional().IsUnicode(false).HasMaxLength(128);
            Property(x => x.ApplicationGroupId).HasColumnName("ApplicationGroupID").IsOptional();
            Property(x => x.ConDbTable).HasColumnName("ConDBTable").IsOptional().IsUnicode(false).HasMaxLength(128);
            Property(x => x.IsMandatorySync).HasColumnName("IsMandatorySync").IsOptional();
            Property(x => x.LastModified).HasColumnName("last_modified").IsOptional();

            HasOptional(a => a.TblApplicationGroup).WithMany(b => b.TblGroupTables).HasForeignKey(c => c.ApplicationGroupId);
        }
    }

    internal class TblSyncArticleConfiguration : EntityTypeConfiguration<TblSyncArticle>
    {
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public TblSyncArticleConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".tblSyncArticles");
            HasKey(x => new { x.SyncArticleId, x.LastModified });

            Property(x => x.SyncArticleId).HasColumnName("SyncArticleID").IsRequired();
            Property(x => x.PublicationName).HasColumnName("PublicationName").IsOptional().IsUnicode(false).HasMaxLength(100);
            Property(x => x.SchemaName).HasColumnName("SchemaName").IsOptional().IsUnicode(false).HasMaxLength(128);
            Property(x => x.TableName).HasColumnName("TableName").IsOptional().IsUnicode(false).HasMaxLength(128);
            Property(x => x.ActiveYn).HasColumnName("ActiveYN").IsOptional();
            Property(x => x.CreatedBy).HasColumnName("CreatedBy").IsOptional();
            Property(x => x.CreatedDateTime).HasColumnName("CreatedDateTime").IsOptional();
            Property(x => x.EditedBy).HasColumnName("EditedBy").IsOptional();
            Property(x => x.EditedDateTime).HasColumnName("EditedDateTime").IsOptional();
            Property(x => x.LastModified).HasColumnName("last_modified").IsRequired();
        }
    }

    internal class TblSyncDetailConfiguration : EntityTypeConfiguration<TblSyncDetail>
    {
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public TblSyncDetailConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".tblSyncDetails");
            HasKey(x => x.ObjectId);

            Property(x => x.ObjectId).HasColumnName("ObjectID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Objectname).HasColumnName("Objectname").IsOptional().IsUnicode(false).HasMaxLength(50);
            Property(x => x.LastSyncVersion).HasColumnName("LastSyncVersion").IsOptional();
            Property(x => x.TruncTimeAtCdb).HasColumnName("TruncTimeAtCDB").IsOptional();
        }
    }


    // ************************************************************************
    // Stored procedure return models

    public class UspCheckGlobalSyncReturnModel
    {
        public Int32? Sync { get; set; }
    }

}

