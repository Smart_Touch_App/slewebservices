﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.Configuration;

namespace LogLib
{
    public class LogDL
    {
        private static string SLEConnection = "SLEEntities";
        private SqlConnection conn = null;

        public LogDL()
        {
            string connectionString = ConfigurationManager.ConnectionStrings[SLEConnection].ConnectionString;
            conn = new SqlConnection(connectionString);
            conn.Open();
        }

        public bool LogResponse(LogDetails objResp)
        {
            string ReqXML = CreateXML(objResp.Request);
            string RespXML = CreateXML(objResp.Resposne);

            SqlCommand cmd = new SqlCommand("SaveWebServiceLog", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("DeviceID", objResp.DeviceID);
            cmd.Parameters.AddWithValue("Request", ReqXML);
            cmd.Parameters.AddWithValue("Response", RespXML);
            cmd.Parameters.AddWithValue("Environment", objResp.Environment);
            cmd.Parameters.AddWithValue("ServiceName", objResp.ServiceName);
            cmd.Parameters.AddWithValue("ServiceEndPoint", objResp.ServiceEndPoint);
            cmd.Parameters.AddWithValue("ServiceContract", objResp.ServiceContract);
            using (conn)
            {
                cmd.ExecuteNonQuery();
            }
            return true;
        }

        #region Private methods
        private string CreateXML(object obj)
        {
            XmlDocument xmlDoc = new XmlDocument();   //Represents an XML document, 
            // Initializes a new instance of the XmlDocument class.          
            XmlSerializer xmlSerializer = new XmlSerializer(obj.GetType());
            // Creates a stream whose backing store is memory. 
            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializer.Serialize(xmlStream, obj);
                xmlStream.Position = 0;
                //Loads the XML document from the specified string.
                xmlDoc.Load(xmlStream);
                return xmlDoc.InnerXml;
            }
        }
        #endregion
    }
}
