﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogLib
{
    public class LogDetails
    {
        public LogDetails(string deviceId, object request, object response, string environment, string servicename, string endpoint, string contract)
        {
            DeviceID = deviceId; Request = request; Resposne = response; Environment = environment; ServiceName = servicename; ServiceEndPoint = endpoint; ServiceContract = contract;
        }

        public string DeviceID { get; set; }
        public object Request { get; set; }
        public object Resposne { get; set; }
        public string Environment { get; set; }
        public string ServiceName { get; set; }
        public string ServiceEndPoint { get; set; }
        public string ServiceContract { get; set; }
    }
}
