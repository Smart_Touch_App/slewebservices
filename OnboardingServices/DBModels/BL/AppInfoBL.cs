﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnboardModels;

namespace DBModels
{
    public class AppInfoBL
    {
        public List<Application> GetAppInfo(string AppName, string EnvName)
        {
            List<Application> objResponse = new List<Application>();
            try
            {
                AppInfoDL objAppInfoDL = new AppInfoDL();
                return objAppInfoDL.GetAppInformation(AppName, EnvName);
            }
            catch (Exception ex)
            {
                return objResponse;
            }

        }
    }
}
