﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnboardModels;

namespace DBModels
{
    public class AuthorizeBL
    {
        public AuthorizeResponse GetEnvironmentRoutes(AuthorizeRequest AuthReq)
        {
            AuthorizeResponse objResponse = new AuthorizeResponse();
            try
            {
                AuthorizeDL objAuthorizeDL = new AuthorizeDL();
                return objAuthorizeDL.GetEnvironmentRoutes(AuthReq);
            }
            catch (Exception ex)
            {
                return objResponse;
            }

        }
    }
}
