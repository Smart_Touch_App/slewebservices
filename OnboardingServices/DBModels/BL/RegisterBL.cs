﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnboardModels;

namespace DBModels
{
    public class RegisterBL
    {
        public RegisterResponse RegisterEnvRouteForUser(RegisterRequest RegReq)
        {
            RegisterResponse objResponse = new RegisterResponse();
            try
            {
                RegisterDL objRegisterDL = new RegisterDL();
                return objRegisterDL.RegisterEnvRouteForUser(RegReq);
            }
            catch (Exception ex)
            {
                return objResponse;
            }

        }
    }
}
