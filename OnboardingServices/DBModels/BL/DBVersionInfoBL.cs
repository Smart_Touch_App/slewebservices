﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnboardModels;

namespace DBModels
{
    public class DBVersionInfoBL
    {
        public DBVersionResponse GetDBVersionInfo(DBVersionRequest UserReq)
        {
            DBVersionResponse objResponse = new DBVersionResponse();
            try
            {
                DBVersionInfoDL objDBVersionDL = new DBVersionInfoDL();
                return objDBVersionDL.GetDBVersionInfo(UserReq);
            }
            catch (Exception ex)
            {
                return objResponse;
            }

        }
    }
}
