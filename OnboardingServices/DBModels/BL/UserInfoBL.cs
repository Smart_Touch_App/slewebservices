﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnboardModels;

namespace DBModels
{
    public class UserInfoBL
    {
        public UserInfoResponse GetUserInformation(UserInfoRequest UserReq)
        {
            UserInfoResponse objResponse = new UserInfoResponse();
            try
            {
                UserInfoDL objUserInfoDL = new UserInfoDL();
                return objUserInfoDL.GetUserInformation(UserReq);
            }
            catch (Exception ex)
            {
                return objResponse;
            }

        }
    }
}
