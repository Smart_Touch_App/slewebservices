﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnboardModels;

namespace DBModels
{
    public class ReserveBL
    {
        public ReserveResponse ReserveEnvRouteForUser(ReserveRequest ResReq)
        {
            ReserveResponse objResponse = new ReserveResponse();
            try
            {
                ReserveDL objReserveDL = new ReserveDL();
                return objReserveDL.ReserveEnvRouteForUser(ResReq);
            }
            catch (Exception ex)
            {
                return objResponse;
            }

        }
    }
}
