﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnboardModels;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace DBModels
{
    public class ReserveDL
    {
        private static string SLEConnection = "SLEEnvDB";
        private SqlConnection conn = null;
        private SqlDataReader reader = null;

        public ReserveDL()
        {
            string connectionString = ConfigurationManager.ConnectionStrings[SLEConnection].ConnectionString;
            conn = new SqlConnection(connectionString);
            conn.Open();
        }

        public ReserveResponse ReserveEnvRouteForUser(ReserveRequest RsrvReq)
        {
            ReserveResponse objResponse = new ReserveResponse();
            bool _isRouteExist = false;
            try
            {
                #region Environment Connection
                string cdbConnection = "";
                SqlCommand cmdEnv = new SqlCommand("SELECT EnvName, CDBConKey FROM busdta.Environment_Master WHERE EnvName=@EnvironmentName", conn);
                cmdEnv.Parameters.AddWithValue("EnvironmentName", RsrvReq.EnvironmentName);
                reader = cmdEnv.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        cdbConnection = Convert.ToString(reader["CDBConKey"]);
                    }
                }
                reader.Close();
                #endregion
                
                #region Check Route Mapped with other
                SqlConnection cdbConn;
                if (!string.IsNullOrEmpty(cdbConnection))
                {
                    string CDBconnectionString = ConfigurationManager.ConnectionStrings[cdbConnection].ConnectionString;
                    cdbConn = new SqlConnection(CDBconnectionString); if (cdbConn.State == ConnectionState.Closed) { cdbConn.Open(); }

                    SqlCommand CDBCommand = new SqlCommand("SELECT RouteName FROM BUSDTA.Route_Master WHERE RouteName=@RouteID", cdbConn);
                    CDBCommand.Parameters.AddWithValue("RouteID", RsrvReq.RouteID);
                    reader = CDBCommand.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            _isRouteExist = true; break;
                        }
                    }
                    reader.Close();

                    if (_isRouteExist)
                    {
                        objResponse = ReserveUserRoute(RsrvReq, cdbConn);
                        return objResponse;
                    }

                    #region return Message
                    objResponse.MessageCode = "109";
                    objResponse.Message = "Route not exist on the environment";
                    objResponse.DeviceID = RsrvReq.DeviceID;
                    objResponse.AppUser = RsrvReq.UserName;
                    objResponse.EnvironmentName = RsrvReq.EnvironmentName;
                    objResponse.RouteID = RsrvReq.RouteID;

                    return objResponse;
                    #endregion
                }
                else
                {
                    #region return Message
                    objResponse.MessageCode = "503";
                    objResponse.Message = "Environment Connection key not exist (CDBConKey in BUSDTA.Environment_Master)";
                    return objResponse;
                    #endregion
                }
                #endregion
            }
            catch (Exception ex)
            {
                return objResponse;
            }
        }

        #region Private Method - Reserve Route
        private ReserveResponse ReserveUserRoute(ReserveRequest RsrvReq, SqlConnection cdbConn)
        {
            ReserveResponse objResponse = new ReserveResponse();
            string _device = "", _user = "", _environment = "", _route = "", _activeYN = "Y"; bool _isRouteReserved = false, _isDeviceReserved = false; DateTime? _reservedOn = DateTime.Now.Date;
            using (conn)
            {
                try
                {
                    SqlCommand cmdcheck = new SqlCommand("uspCheckRouteAvailabilityForReserve", conn);
                    cmdcheck.CommandType = CommandType.StoredProcedure;
                    reader = cmdcheck.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            #region Existing Route values

                            _user = Convert.ToString(reader["App_User"]);
                            _environment = Convert.ToString(reader["EnvName"]);
                            _activeYN = Convert.ToString(reader["ActiveYN"]).ToUpper();
                            _device = Convert.ToString(reader["DeviceID"]);
                            _route = Convert.ToString(reader["RouteID"]);
                            _reservedOn = (reader["ReservedDateTime"] == DBNull.Value || reader["ReservedDateTime"] == "") ? (DateTime?)null : Convert.ToDateTime(reader["ReservedDateTime"]);

                            if ((Convert.ToString(reader["DeviceID"]) == RsrvReq.DeviceID) && _environment == RsrvReq.EnvironmentName && _reservedOn != (DateTime?)null)
                            {
                                _isDeviceReserved = true;
                            }
                            if ((Convert.ToString(reader["RouteID"]) == RsrvReq.RouteID) && _environment == RsrvReq.EnvironmentName && _reservedOn != (DateTime?)null)
                            {
                                _isRouteReserved = true;
                            }
                            if ((_device == RsrvReq.DeviceID || _route == RsrvReq.RouteID) && _environment == RsrvReq.EnvironmentName && _activeYN=="Y")
                            {
                                _isRouteReserved = true; _isDeviceReserved = true;
                                break;
                            }
                            #endregion
                        }
                    }
                    reader.Close();

                    Int32? _remoteDBID = null;
                    if (!_isRouteReserved && !_isDeviceReserved)
                    {
                        #region Reserve Environment at SystemDB
                        SqlCommand cmd = new SqlCommand("Reserve_DeviceEnvironmentRoute", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("DeviceId", RsrvReq.DeviceID);
                        cmd.Parameters.AddWithValue("EnvironmentName", RsrvReq.EnvironmentName);
                        cmd.Parameters.AddWithValue("RouteID", RsrvReq.RouteID);
                        cmd.Parameters.AddWithValue("AppUserName", RsrvReq.UserName);
                        reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            objResponse.MessageCode = "101";
                            objResponse.Message = "The Environment and Route is reserved for the device";
                            objResponse.DeviceID = RsrvReq.DeviceID;
                            objResponse.AppUser = RsrvReq.UserName;
                            objResponse.EnvironmentName = RsrvReq.EnvironmentName;
                            objResponse.RouteID = RsrvReq.RouteID;
                            objResponse.SetupURL = new List<Application>();
                            while (reader.Read())
                            {
                                #region Existing Route values
                                if (reader["RemoteDBID"] != null && !string.IsNullOrWhiteSpace(Convert.ToString(reader["RemoteDBID"])))
                                    _remoteDBID = Convert.ToInt32(reader["RemoteDBID"]);

                                Application objApp = new Application();
                                objApp.AppType = Convert.ToString(reader["AppType"]);
                                objApp.AppName = Convert.ToString(reader["AppName"]);
                                objApp.AppLocation = Convert.ToString(reader["AppLocation"]);

                                if (reader["ReleasedDate"] != null)
                                    objApp.ReleasedDate = Convert.ToDateTime(reader["ReleasedDate"]);

                                if (_remoteDBID!=null)
                                    objResponse.RemoteDbID = _remoteDBID.ToString();

                                objResponse.SetupURL.Add(objApp);
                                #endregion
                            }
                        }
                        reader.Close();

                        #endregion

                        #region Reserve Route at ConsDB
                        SqlCommand cmdCons = new SqlCommand("Reserve_DeviceRoute", cdbConn);//At Consolidated db - same name
                        cmdCons.CommandType = CommandType.StoredProcedure;
                        cmdCons.Parameters.AddWithValue("DeviceId", RsrvReq.DeviceID);
                        cmdCons.Parameters.AddWithValue("RouteID", RsrvReq.RouteID);
                        cmdCons.Parameters.AddWithValue("DBID", objResponse.RemoteDbID);
                        int rslt = cmdCons.ExecuteNonQuery();
                        #endregion
                    }
                    else
                    {
                        #region return Message
                        string _congfigMsg = "reserved";
                        if (_activeYN == "Y")
                        {
                            _congfigMsg = "registered";
                        }
                        if (_device != RsrvReq.DeviceID)
                        {
                            objResponse.MessageCode = "103";
                            objResponse.Message = "The \"RouteID\" and \"Environment\" is already " + _congfigMsg + " with another device.";
                        }
                        else
                        {
                            if (_isDeviceReserved && _route !=RsrvReq.RouteID)
                            {
                                objResponse.MessageCode = "105";
                                objResponse.Message = "The device is already " + _congfigMsg + " with another route.";
                            }
                            else
                            {
                                objResponse.MessageCode = "106";
                                objResponse.Message = "The \"RouteID\" and \"Environment\" is already " + _congfigMsg + " for the same device.";
                            }
                        }

                        objResponse.AppUser = _user;
                        objResponse.DeviceID = _device;
                        objResponse.EnvironmentName = _environment;
                        objResponse.RouteID = _route;
                        objResponse.SetupURL = new List<Application>();
                        #endregion
                    }
                }

                catch (Exception ex)
                {
                    objResponse.MessageCode = "504";
                    objResponse.Message = ex.Message;
                }
                finally
                {
                }
            }
            return objResponse;
        }
        #endregion
    }
}