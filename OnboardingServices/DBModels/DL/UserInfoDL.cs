﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnboardModels;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace DBModels
{
    public class UserInfoDL
    {
        private static string SLEConnection = "SLEEnvDB";
        private SqlConnection conn = null;
        private SqlDataReader reader = null;

        public UserInfoDL()
        {
            string connectionString = ConfigurationManager.ConnectionStrings[SLEConnection].ConnectionString;
            conn = new SqlConnection(connectionString);
            conn.Open();
        }

        public UserInfoResponse GetUserInformation(UserInfoRequest UserInfoReq)
        {
            UserInfoResponse objResponse = new UserInfoResponse();
            try
            {
                objResponse = getUserDetails(string.Concat(UserInfoReq.DomainName, @"\", UserInfoReq.DomainUser));

                return objResponse;
            }
            catch (Exception ex)
            {
                return objResponse;
            }
        }
        
        #region Private methods

        private UserInfoResponse getUserDetails(string DomainUser)
        {
            UserInfoResponse objResponse = new UserInfoResponse();
            using (conn)
            {
                try
                {
                    SqlCommand cmdCheckDeviceMaped = new SqlCommand("UserDetail", conn);
                    cmdCheckDeviceMaped.CommandType = CommandType.StoredProcedure;
                    cmdCheckDeviceMaped.Parameters.AddWithValue("DomainUser", DomainUser);
                    reader = cmdCheckDeviceMaped.ExecuteReader();
                    if (reader.HasRows)
                    {
                        // loop thru the result to get details
                        while (reader.Read())
                        {
                            objResponse.App_user_id = Convert.ToInt32(reader["App_user_id"]);
                            objResponse.App_User = Convert.ToString(reader["App_User"]);
                            objResponse.Name = Convert.ToString(reader["Name"]);
                            objResponse.DomainUser = Convert.ToString(reader["DomainUser"]);
                            //objResponse.AppPassword = Convert.ToString(reader["AppPassword"]);
                            objResponse.active = Convert.ToBoolean(reader["active"]);
                            break;
                        }
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }

            return objResponse;
        }
        #endregion

    }
}
