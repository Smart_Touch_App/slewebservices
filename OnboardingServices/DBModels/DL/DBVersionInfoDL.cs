﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnboardModels;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace DBModels
{
    public class DBVersionInfoDL
    {
        private static string SLEConnection = "SLEEnvDB";
        private SqlConnection conn = null;
        private SqlDataReader reader = null;

        public DBVersionInfoDL()
        {
            string connectionString = ConfigurationManager.ConnectionStrings[SLEConnection].ConnectionString;
            conn = new SqlConnection(connectionString);
            conn.Open();
        }

        public DBVersionResponse GetDBVersionInfo(DBVersionRequest DBVersionReq)
        {
            DBVersionResponse objResponse = new DBVersionResponse();
            try
            {
                objResponse = getDBVersion(DBVersionReq.RemoteCDBVersion, DBVersionReq.RemoteSysDBVersion);

                return objResponse;
            }
            catch (Exception ex)
            {
                return objResponse;
            }
        }
        
        #region Private methods

        private DBVersionResponse getDBVersion(string CDBVersion, string SysDBVersion)
        {
            DBVersionResponse objResponse = new DBVersionResponse();
            using (conn)
            {
                try
                {
                    SqlCommand cmdCheckDeviceMaped = new SqlCommand("uspGetRemoteDBInfo", conn);
                    cmdCheckDeviceMaped.CommandType = CommandType.StoredProcedure;
                    cmdCheckDeviceMaped.Parameters.AddWithValue("CdbVersion", CDBVersion);
                    cmdCheckDeviceMaped.Parameters.AddWithValue("SysDBVersion", SysDBVersion);
                    reader = cmdCheckDeviceMaped.ExecuteReader();
                    if (reader.HasRows)
                    {
                        // loop thru the result to get details
                        while (reader.Read())
                        {
                            objResponse.ReleaseName = Convert.ToString(reader["ReleaseName"]);
                            objResponse.SysDBVersion = Convert.ToString(reader["SysDBVersion"]);
                            objResponse.CDBVersion = Convert.ToString(reader["CDBVersion"]);
                            break;
                        }
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }

            return objResponse;
        }
        #endregion

    }
}
