﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnboardModels;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.ServiceModel;

namespace DBModels
{
    public class RegisterDL
    {
        private static string SLEConnection = "SLEEnvDB";
        private SqlConnection conn = null;
        private SqlDataReader reader = null;

        public RegisterDL()
        {
            string connectionString = ConfigurationManager.ConnectionStrings[SLEConnection].ConnectionString;
            conn = new SqlConnection(connectionString);
            conn.Open();
        }

        public RegisterResponse RegisterEnvRouteForUser(RegisterRequest RegReq)
        {
            RegisterResponse objResponse = new RegisterResponse();
            try
            {
                objResponse = RegisterUserRoute(RegReq);

                return objResponse;
            }
            catch (Exception ex)
            {
                return objResponse;
            }
        }

        #region Private Method - Register Route
        private RegisterResponse RegisterUserRoute(RegisterRequest RegReq)
        {
            RegisterResponse objResponse = new RegisterResponse();

            string _ActiveYN = "N", _deviceID = "", _envName = "", _routeID = ""; DateTime _reservedOn;
            Dictionary<string, OnboardModels.Environment> envListDict = new Dictionary<string, OnboardModels.Environment>(); bool _isRouteRegistered = false;
            using (conn)
            {
                try
                {
                    #region Check the reservation exists
                    int _ReservationID = 0;
                    //SqlCommand cmdUser = new SqlCommand("SELECT ISNULL(ReservationID,0) AS ReservationID, rdb.DeviceID, em.EnvName, rdb.RouteID, rdb.ActiveYN FROM BUSDTA.RemoteDBID_Master AS rdb LEFT JOIN BUSDTA.Reserve_DeviceEnvironmentRoute AS der ON rdb.DeviceID=der.DeviceID AND rdb.EnvironmentMasterID=der.EnvironmentMasterID AND rdb.RouteID=der.RouteID LEFT JOIN BUSDTA.Environment_Master AS em ON em.EnvironmentMasterId=rdb.EnvironmentMasterId  WHERE rdb.DeviceId=@DeviceId", conn);
                    SqlCommand cmdUser = new SqlCommand("uspValidateRegisteration", conn);
                    cmdUser.CommandType = CommandType.StoredProcedure;
                    cmdUser.Parameters.AddWithValue("DeviceId", RegReq.DeviceID);
                    reader = cmdUser.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            _ReservationID = Convert.ToInt32(reader["ReservationID"]);
                            _deviceID = Convert.ToString(reader["DeviceID"]);
                            _envName = Convert.ToString(reader["EnvName"]);
                            _routeID = Convert.ToString(reader["RouteID"]);
                            _ActiveYN = Convert.ToString(reader["ActiveYN"]);
                            if (!string.IsNullOrEmpty(Convert.ToString(reader["ReservedDateTime"])))
                            {
                                _reservedOn = Convert.ToDateTime(reader["ReservedDateTime"]);
                            }
                            break;
                        }
                    }
                    reader.Close();
                    #endregion
                    if (_ReservationID > 0)
                    {
                        #region Check Device Registered with Route and environment
                        SqlCommand cmdRegisterd = new SqlCommand("SELECT DeviceID, 1 AS Registered, ActiveYN FROM BUSDTA.RemoteDBID_Master AS RM INNER JOIN BUSDTA.Environment_Master AS EM ON RM.EnvironmentMasterID=EM.EnvironmentMasterId WHERE RouteID=@RouteID AND EnvName=@EnvName", conn);
                        cmdRegisterd.Parameters.AddWithValue("RouteID", RegReq.RouteID);
                        cmdRegisterd.Parameters.AddWithValue("EnvName", RegReq.EnvironmentName);
                        reader = cmdRegisterd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                _deviceID = Convert.ToString(reader["DeviceID"]);
                                _isRouteRegistered = Convert.ToBoolean(reader["Registered"]);
                                _ActiveYN = Convert.ToString(reader["ActiveYN"]);
                                break;
                            }
                        }
                        reader.Close();
                        #endregion

                        if (_isRouteRegistered == true && _ActiveYN != "Y")
                        {
                            #region Environment Connection
                            string cdbConnection = "";
                            SqlCommand cmdEnv = new SqlCommand("SELECT EnvName, CDBConKey FROM busdta.Environment_Master WHERE EnvName=@EnvironmentName", conn);
                            cmdEnv.Parameters.AddWithValue("EnvironmentName", RegReq.EnvironmentName);
                            reader = cmdEnv.ExecuteReader();
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    cdbConnection = Convert.ToString(reader["CDBConKey"]); break;
                                }
                            }
                            reader.Close();
                            #endregion

                            #region Check Route Mapped with other
                            SqlConnection cdbConn;
                            if (!string.IsNullOrEmpty(cdbConnection))
                            {
                                string CDBconnectionString = ConfigurationManager.ConnectionStrings[cdbConnection].ConnectionString;
                                cdbConn = new SqlConnection(CDBconnectionString); if (cdbConn.State == ConnectionState.Closed) { cdbConn.Open(); }

                                #region Add Environment and Route
                                #region Add Environment
                                SqlCommand Syscmd = new SqlCommand("Register_DeviceEnvironment", conn);
                                Syscmd.CommandType = CommandType.StoredProcedure;
                                Syscmd.Parameters.AddWithValue("DeviceId", RegReq.DeviceID);
                                Syscmd.Parameters.AddWithValue("RouteID", RegReq.RouteID);
                                Syscmd.Parameters.AddWithValue("EnvironmentName", RegReq.EnvironmentName);
                                Syscmd.Parameters.AddWithValue("AppUserName", RegReq.UserName);
                                int SysResult = Syscmd.ExecuteNonQuery();
                                #endregion

                                #region Add Route
                                if (SysResult >= 0)
                                {
                                    SqlCommand cmd = new SqlCommand("Register_DeviceRoute", cdbConn);
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Parameters.AddWithValue("DeviceId", RegReq.DeviceID);
                                    cmd.Parameters.AddWithValue("RouteID", RegReq.RouteID);
                                    int result = cmd.ExecuteNonQuery();

                                    if (result > 0)
                                    {
                                        objResponse.MessageCode = "101";
                                        objResponse.Message = "The route successfully registered";
                                    }
                                }
                                #endregion
                                #endregion
                            }
                            else
                            {
                                #region return Message
                                objResponse.MessageCode = "505";
                                objResponse.Message = "Environment Connection key not exist (CDBConKey in BUSDTA.Environment_Master)";
                                #endregion
                            }
                            #endregion
                        }
                        //else if (_ActiveYN == "Y")
                        //{
                        //    #region return Message
                        //    objResponse.MessageCode = "106";
                        //    objResponse.RegistrationStatus = "The Environment and Route is not reserved for the device";
                        //    #endregion
                        //}
                        else if (!_isRouteRegistered)
                        {
                            #region return Message
                            objResponse.MessageCode = "106";
                            objResponse.Message = "The Device and Route combination does not have a current valid reservation.";
                            #endregion
                        }
                        else 
                        {
                            #region return Message
                            objResponse.MessageCode = "107";
                            objResponse.Message = "The Environment and Route is already registered";
                            objResponse.RegisterationParms = RegReq;
                            if (_deviceID == RegReq.DeviceID) { objResponse.Message += " with the same device"; }
                            #endregion
                        }
                    }
                    else if (_ActiveYN == "Y")
                    {
                        #region return Message
                        objResponse.MessageCode = "108";
                        objResponse.Message = "The environment and route is already registered";
                        if (RegReq.DeviceID == _deviceID && _routeID==RegReq.RouteID)
                        {
                            objResponse.Message += " with the same device.";
                        }
                        else if (_routeID == RegReq.RouteID)
                        {
                            objResponse.Message += " with another device.";
                        }
                        else if (_routeID != RegReq.RouteID)
                        {
                            objResponse.Message += " with another route.";
                        }
                        #endregion
                    }
                    else
                    {
                        #region return Message
                        objResponse.MessageCode = "106";
                        objResponse.Message = "The Device and Route combination does not have a current valid reservation.";
                        #endregion
                    }
                    
                }

                catch (Exception ex)
                {
                    objResponse.MessageCode = "506";
                    objResponse.Message = ex.Message;
                }
                finally
                {
                }
            }

            return objResponse;
        }
        #endregion
    }
}
