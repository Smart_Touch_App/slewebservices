﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnboardModels;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace DBModels
{
    public class AuthorizeDL
    {
        private static string SLEConnection = "SLEEnvDB";
        private SqlConnection conn = null;
        private SqlDataReader reader = null;

        public AuthorizeDL()
        {
            string connectionString = ConfigurationManager.ConnectionStrings[SLEConnection].ConnectionString;
            conn = new SqlConnection(connectionString);
            conn.Open();
        }

        public AuthorizeResponse GetEnvironmentRoutes(AuthorizeRequest AuthReq)
        {
            AuthorizeResponse objResponse = new AuthorizeResponse();
            try
            {
                objResponse = getEnvRouteList(AuthReq.UserName, AuthReq.DeviceID);

                return objResponse;
            }
            catch (Exception ex)
            {
                return objResponse;
            }
        }
        
        #region Private methods

        private AuthorizeResponse getEnvRouteList(string AppUser, string DeviceID)
        {
            AuthorizeResponse objResponse = new AuthorizeResponse();
            List<OnboardModels.Environment> objEnvList = new List<OnboardModels.Environment>();
            Dictionary<string, OnboardModels.Environment> envListDict = new Dictionary<string, OnboardModels.Environment>();
            using (conn)
            {
                try
                {
                    bool _isDeviceRegistered = false, _isActiveDevice = false, _isDeviceExist = false; string _deviceID = "";
                    SqlCommand cmdCheckDeviceMaped = new SqlCommand("ValidateDevice", conn);
                    cmdCheckDeviceMaped.CommandType = CommandType.StoredProcedure;
                    cmdCheckDeviceMaped.Parameters.AddWithValue("DeviceID", DeviceID);
                    reader = cmdCheckDeviceMaped.ExecuteReader();
                    if (reader.HasRows)
                    {
                        // loop thru the result to get details
                        while (reader.Read())
                        {
                            _deviceID = Convert.ToString(reader["DeviceID"]);
                            _isDeviceRegistered = Convert.ToString(reader["IsRegistered"]) == "Y" ? true : false;
                            _isActiveDevice = Convert.ToString(reader["IsDeviceActive"]) == "1" ? true : false;
                            _isDeviceExist = true;
                            break;
                        }
                    }
                    reader.Close();

                    if (_isActiveDevice && !_isDeviceRegistered && _isDeviceExist)
                    {
                        #region Get environment-route List
                        SqlCommand cmd = new SqlCommand("GetMappedEnv", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("AppUser", AppUser);
                        reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            // loop thru the result to get details
                            while (reader.Read())
                            {
                                #region Environment List
                                OnboardModels.Environment env = new OnboardModels.Environment();
                                env.EnvironmentId = Convert.ToString(reader["EnvironmentMasterId"]);
                                env.EnvironmentName = Convert.ToString(reader["EnvName"]);
                                env.EnvironmentDescription = Convert.ToString(reader["EnvDescription"]);
                                env.AppUserID = Convert.ToString(reader["App_User_ID"]);
                                env.AppUserName = Convert.ToString(reader["App_User"]);
                                #endregion

                                #region Get CDB(Env)Conneciton
                                string cdbConnection = Convert.ToString(reader["CDBConKey"]);
                                envListDict.Add(cdbConnection, env);
                                #endregion
                            }
                            reader.Close();
                        }

                        foreach (var cdbconstr in envListDict)
                        {
                            List<OnboardModels.Route> objRouteList = new List<OnboardModels.Route>();
                            string conStr = cdbconstr.Key;

                            OnboardModels.Environment env = cdbconstr.Value;
                            if (!string.IsNullOrEmpty(conStr))
                            {
                                string CDBconnectionString = ConfigurationManager.ConnectionStrings[conStr].ConnectionString;
                                SqlConnection cdbConn = new SqlConnection(CDBconnectionString); if (cdbConn.State == ConnectionState.Closed) { cdbConn.Open(); }

                                SqlCommand CDBCommand = new SqlCommand("SELECT RUM.Route_Id, RM.RouteDescription, RM.DefaultUser, RS.RouteStatus, RS.StatusDescription FROM BUSDTA.Route_User_Map AS RUM INNER JOIN BUSDTA.Route_Master AS RM ON RUM.Route_Id=RM.RouteName LEFT JOIN BUSDTA.RouteStatus AS RS ON RM.RouteStatusID=RS.RouteStatusID WHERE RUM.App_User_id=@AppUser", cdbConn);
                                CDBCommand.Parameters.AddWithValue("AppUser", env.AppUserID);
                                reader = CDBCommand.ExecuteReader();
                                if (reader.HasRows)
                                {
                                    // loop thru the result to get details
                                    while (reader.Read())
                                    {
                                        #region Route
                                        OnboardModels.Route objrt = new OnboardModels.Route();
                                        objrt.RouteId = Convert.ToString(reader["Route_Id"]);
                                        objrt.RouteDescription = Convert.ToString(reader["RouteDescription"]);
                                        objrt.DefaultUser = Convert.ToString(reader["DefaultUser"]);
                                        objrt.RouteStatus = Convert.ToString(reader["RouteStatus"]);
                                        objrt.RouteStatusDescription = Convert.ToString(reader["StatusDescription"]);
                                        objRouteList.Add(objrt);
                                        #endregion
                                    }
                                    env.RouteList = objRouteList;
                                }
                                reader.Close();
                            }

                            objEnvList.Add(env);
                        }
                        #endregion

                        objResponse.MessageCode = "101";
                        objResponse.Message = "Device is available for registeration.";
                        objResponse.EnvironmentList = objEnvList;
                    }
                    else
                    {
                        if (_isDeviceRegistered)
                        {
                            objResponse.MessageCode = "102";
                            objResponse.Message = "The device is already registered with a route.";
                        }
                        else if (!_isDeviceExist)
                        {
                            objResponse.MessageCode = "401";
                            objResponse.Message = "Invalid DeviceID.";
                        }
                        else if (!_isActiveDevice)
                        {
                            objResponse.MessageCode = "502";
                            objResponse.Message = "The device is not active.";
                        }
                    }
                }
                catch (Exception ex)
                {
                    objResponse.MessageCode = "501";
                    objResponse.Message = ex.Message;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }

            return objResponse;
        }
        #endregion

    }
}
