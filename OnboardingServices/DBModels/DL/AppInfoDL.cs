﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnboardModels;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace DBModels
{
    public class AppInfoDL
    {
        private static string SLEConnection = "SLEEnvDB";
        private SqlConnection conn = null;
        private SqlDataReader reader = null;

        public AppInfoDL()
        {
            string connectionString = ConfigurationManager.ConnectionStrings[SLEConnection].ConnectionString;
            conn = new SqlConnection(connectionString);
            conn.Open();
        }

        public List<Application> GetAppInformation(string AppName, string EnvName)
        {
            List<Application> objResponse = new List<Application>();
            try
            {
                objResponse = getAppDetails(AppName, EnvName);

                return objResponse;
            }
            catch (Exception ex)
            {
                return objResponse;
            }
        }
        
        #region Private methods

        private List<Application> getAppDetails(string AppName, string EnvName)
        {
            List<Application> objResponseList = new List<Application>();
            using (conn)
            {
                try
                {
                    SqlCommand cmdCheckDeviceMaped = new SqlCommand("uspGetAppVersion", conn);
                    cmdCheckDeviceMaped.CommandType = CommandType.StoredProcedure;
                    cmdCheckDeviceMaped.Parameters.AddWithValue("EnvironmentName", EnvName);
                    cmdCheckDeviceMaped.Parameters.AddWithValue("AppName", AppName);
                    
                    reader = cmdCheckDeviceMaped.ExecuteReader();
                    if (reader.HasRows)
                    {
                        // loop thru the result to get details
                        while (reader.Read())
                        {
                            Application objResponse = new Application();
                            objResponse.EnvironmentName = Convert.ToString(reader[0]);
                            objResponse.AppName = Convert.ToString(reader[1]);
                            objResponse.AppType = Convert.ToString(reader[2]);
                            objResponse.AppVersion = Convert.ToString(reader[3]);
                            objResponse.ReleasedDate = Convert.ToDateTime(reader[4]);
                            objResponse.AppLocation = Convert.ToString(reader[5]);
                            objResponse.IsMandatory = Convert.ToBoolean(reader[6]);
                            objResponseList.Add(objResponse);
                        }
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }

            return objResponseList;
        }
        #endregion

    }
}
