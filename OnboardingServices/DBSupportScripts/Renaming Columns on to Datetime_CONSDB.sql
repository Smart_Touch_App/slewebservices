/*
EnvDB- ConsolidatedDB
*/
select * from sys.tables where type='U'

select object_name(object_id), * from sys.columns
where object_id in(
	select object_id from sys.tables where type='U'
)
and system_type_id=167 and name in('CreatedBy', 'EditedBy')

--select * from sys.types where system_type_id=167

--update tblSystemSetup SET CreatedBy=1, EditedBy=1
--update NonMobilinkSource SET CreatedBy=1, EditedBy=1
--update tblSyncArticles SET CreatedBy=1, EditedBy=1
--GO

--ALTER TABLE tblSystemSetup ALTER COLUMN CreatedBy INT
--ALTER TABLE tblSystemSetup ALTER COLUMN EditedBy INT
--ALTER TABLE NonMobilinkSource ALTER COLUMN CreatedBy INT
--ALTER TABLE NonMobilinkSource ALTER COLUMN EditedBy INT
--ALTER TABLE tblSyncArticles ALTER COLUMN CreatedBy INT
--ALTER TABLE tblSyncArticles ALTER COLUMN EditedBy INT


select object_name(object_id), * from sys.columns
where object_id in(
	select object_id from sys.tables where type='U'
)
and name in('CreatedOn', 'EditedOn')
GO

SP_RENAME 'tblSystemSetup.CreatedOn', 'CreatedDateTime', 'COLUMN'
GO
SP_RENAME 'tblSystemSetup.EditedOn', 'EditedDateTime', 'COLUMN'
GO
SP_RENAME 'NonMobilinkSource.CreatedOn', 'CreatedDateTime', 'COLUMN'
GO
SP_RENAME 'NonMobilinkSource.EditedOn', 'EditedDateTime', 'COLUMN'
GO
SP_RENAME 'tblGlobalSync.CreatedOn', 'CreatedDateTime', 'COLUMN'
GO
SP_RENAME 'tblGlobalSyncLog.CreatedOn', 'CreatedDateTime', 'COLUMN'
GO
SP_RENAME 'tblSyncArticles.EditedOn', 'EditedDateTime', 'COLUMN'
GO
SP_RENAME 'tblSyncArticles.CreatedOn', 'CreatedDateTime', 'COLUMN'
GO
SP_RENAME 'BUSDTA.tblTransactionAudit.CreatedOn', 'CreatedDateTime', 'COLUMN'
GO
SP_RENAME 'BUSDTA.tblTransactionControl.CreatedOn', 'CreatedDateTime', 'COLUMN'
GO


SELECT OBJECT_NAME(id),* FROM sys.syscomments where text like'%CreatedOn%'
GO
SELECT OBJECT_NAME(id),* FROM sys.syscomments where text like'%EditedOn%'
GO
ALTER TABLE [dbo].[tblApplicationGroup] ADD  CONSTRAINT [PK_tblApplicationGroup] PRIMARY KEY CLUSTERED 
(
	[ApplicationGroupID] ASC
)
GO

ALTER TABLE [dbo].[tblGroupTables] ADD  CONSTRAINT [PK_tblGroupTables] PRIMARY KEY CLUSTERED 
(
	[GroupTableID] ASC
)
GO
ALTER TABLE [dbo].[tblGroupTables]  WITH CHECK ADD  CONSTRAINT [FK_tblGroupTables_tblApplicationGroup] FOREIGN KEY([ApplicationGroupID])
REFERENCES [dbo].[tblApplicationGroup] ([ApplicationGroupID])
GO

ALTER TABLE [dbo].[tblGroupTables] CHECK CONSTRAINT [FK_tblGroupTables_tblApplicationGroup]
GO

