SELECT * FROM [BUSDTA].[Device_Master]

--INSERT INTO BUSDTA.Device_Master
--(Device_Id, Active, manufacturer, model)
--SELECT TOP 10 Device_Id, Active, manufacturer, model FROM [HP-1\SQLEXPRESS2012].MobileDataModel_FPS4.BUSDTA.Device_Master

SELECT * FROM [BUSDTA].user_master

--INSERT INTO BUSDTA.user_master
--(App_User, Name, DomainUser, AppPassword, active)
--SELECT TOP 10 App_User, Name, DomainUser, AppPassword, active FROM [HP-1\SQLEXPRESS2012].MobileDataModel_FPS4.BUSDTA.user_master

SELECT * FROM BUSDTA.Environment_Master
--INSERT INTO BUSDTA.Environment_Master (EnvName, EnvDescription, IsActive, CDBConKey)
--VALUES ('Dev', 'Development Server','Y','SLECons_Dev')

SELECT * FROM BUSDTA.User_Environment_Map
--INSERT INTO BUSDTA.User_Environment_Map (UserEnvironmentMapId, EnvironmentMasterId, AppUserId, IsEnvAdmin)
--VALUES (1,1,1,1)

SELECT * FROM BUSDTA.Device_Environment_Map
--INSERT INTO BUSDTA.Device_Environment_Map(EnvironmentMasterId, DeviceId)
--SELECT 1 EnvironmentMasterId, Device_Id DeviceId FROM BUSDTA.Device_Master WHERE manufacturer!='Vmware'


CREATE PROCEDURE [dbo].[ValidateDevice]
(
	@DeviceID nvarchar(50) 
)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT DM.Device_ID AS DeviceID, ISNULL(RM.ActiveYN,'N') AS IsRegistered, DM.Active AS IsDeviceActive FROM BUSDTA.Device_Master AS DM
	LEFT JOIN BUSDTA.RemoteDBID_Master AS RM ON RM.DeviceID=DM.Device_ID AND RM.ActiveYN='Y'
	WHERE DM.Device_ID=@DeviceID 
	SET NOCOUNT OFF;
END

GO

CREATE PROCEDURE [dbo].[GetMappedEnv]
(
	@AppUser nvarchar(50) 
)
AS
BEGIN
SET NOCOUNT ON;
	SELECT UM.App_User_ID, UM.App_User, EM.EnvName, EM.EnvDescription, EM.CDBConKey, EM.EnvironmentMasterID
		FROM BUSDTA.user_master UM
		INNER JOIN BUSDTA.User_Environment_Map UEMAP ON UM.App_user_id = UEMAP.AppUserId 
		INNER JOIN BUSDTA.Environment_Master EM ON EM.EnvironmentMasterId = UEMAP.EnvironmentMasterId 
	WHERE UM.App_User = @AppUser
SET NOCOUNT OFF;
END

GO

CREATE PROCEDURE [dbo].[uspCheckRouteAvailabilityForReserve]
AS
BEGIN
	DECLARE @ExpMins INT;
	/*get reservation expire minutes*/
	SELECT @ExpMins = (ExpireMins*-1) FROM tblReserveConfig 

	SELECT rdbM.DeviceID, um.App_User, EnvName, rdbM.RouteID, rdbM.ActiveYN,ReservedOn
	FROM BUSDTA.RemoteDBID_Master AS rdbM
	INNER JOIN BUSDTA.Environment_Master AS em ON rdbM.EnvironmentMasterId=em.EnvironmentMasterId 
	INNER JOIN BUSDTA.User_Master AS um ON rdbM.AppUserId=um.App_user_id
	LEFT JOIN BUSDTA.Reserve_DeviceEnvironmentRoute AS der
		ON rdbM.EnvironmentMasterId=der.EnvironmentMasterId AND rdbM.RouteID=der.RouteID AND rdbM.DeviceID=der.DeviceID AND rdbM.AppUserID=der.AppUserID AND ReservedOn>=DATEADD(MINUTE, @ExpMins, GETDATE())
END
GO

ALTER PROCEDURE [dbo].[Reserve_DeviceEnvironmentRoute]
(
	@DeviceId VARCHAR(30),
	@EnvironmentName VARCHAR(30),
	@RouteID VARCHAR(30),
	@AppUserName VARCHAR(30)
)
AS
BEGIN
DECLARE @EnvironmentMasterId INT, @AppUserId INT, @rdb_script_location VARCHAR(MAX), @RemoteDBID INT;
SELECT @EnvironmentMasterId = EnvironmentMasterId, @rdb_script_location=RDBScriptLocation FROM BUSDTA.Environment_Master WHERE EnvName=@EnvironmentName
SELECT @AppUserId = App_User_ID FROM BUSDTA.User_Master WHERE App_User=@AppUserName
SELECT @RemoteDBID = ISNULL(MAX(RemoteDBID),1000) FROM BUSDTA.RemoteDBID_Master

	IF NOT EXISTS(SELECT 1 FROM BUSDTA.Reserve_DeviceEnvironmentRoute WHERE DeviceId=@DeviceId AND EnvironmentMasterId=@EnvironmentMasterId AND RouteID=@RouteID)
	BEGIN
		INSERT INTO BUSDTA.Reserve_DeviceEnvironmentRoute(DeviceId, EnvironmentMasterId, RouteID, AppUserId)
		SELECT @DeviceId, @EnvironmentMasterId, @RouteID, @AppUserId
	END
	ELSE
	BEGIN
		UPDATE BUSDTA.Reserve_DeviceEnvironmentRoute SET ReservedOn=GETDATE() 
		WHERE DeviceId=@DeviceId AND EnvironmentMasterId=@EnvironmentMasterId AND RouteID=@RouteID
	END

	--IF NOT EXISTS(SELECT 1 FROM BUSDTA.RemoteDBID_Master WHERE DeviceId=@DeviceId AND EnvironmentMasterId=@EnvironmentMasterId AND RouteID=@RouteID)
	BEGIN
		SET @RemoteDBID=@RemoteDBID+1
		INSERT INTO BUSDTA.RemoteDBID_Master (RemoteDBID, AppUserId, DeviceID, RouteID, EnvironmentMasterID)
		VALUES (@RemoteDBID, @AppUserId, @DeviceId, @RouteID,@EnvironmentMasterId)
	END

	SELECT @rdb_script_location RDBScriptLocation, @RemoteDBID RemoteDBID
END
GO

/*
=============================================
Author:			Sakaravarthi J
Create date:	28-Sep-2015
Description:	Logs Webservices' request and response
	Logs WebServices source from all sources
		1. Stats Services
		2. Onboarding Services
Note: this Stored Procedures gets/logs by calling from CDB 
	(where the services configured to the database as the web service contact only CDB)
============================================= 
*/
ALTER PROCEDURE [dbo].[SaveWebServiceLog]
(
	@DeviceID VARCHAR(100)='',
	@Request VARCHAR(MAX),
	@Response VARCHAR(MAX),
	@Environment VARCHAR(128)='',
	@DBName VARCHAR(128)='',
	@ServiceName VARCHAR(100)='',
	@ServiceEndPoint VARCHAR(1000)='',
	@ServiceContract VARCHAR(1000)=''
)
AS
BEGIN
SET NOCOUNT ON
	DECLARE @ServiceLogID INT = 0;
	SELECT @ServiceLogID = ISNULL(MAX(ServiceLogID), 0)+1 FROM tblWebServiceLog;

	INSERT INTO [dbo].[tblWebServiceLog] ([ServiceLogID], [DeviceID], [Request], [Response], [Environment], [DBName], [ServiceName], [ServiceEndPoint], [ServiceContract])
	VALUES(@ServiceLogID, @DeviceID, CAST(@Request AS XML), CAST(@Response AS XML), @Environment, @DBName, @ServiceName, @ServiceEndPoint, @ServiceContract)
SET NOCOUNT OFF
END

GO
ALTER PROCEDURE [dbo].[uspCheckRouteAvailabilityForReserve]
AS
BEGIN
	DECLARE @ExpMins INT;
	/*get reservation expire minutes*/
	SELECT @ExpMins = (ExpireMins*-1) FROM tblReserveConfig 

	SELECT rdbM.DeviceID, um.App_User, EnvName, rdbM.RouteID, rdbM.ActiveYN,ReservedOn
	FROM BUSDTA.RemoteDBID_Master AS rdbM
	INNER JOIN BUSDTA.Environment_Master AS em ON rdbM.EnvironmentMasterId=em.EnvironmentMasterId 
	INNER JOIN BUSDTA.User_Master AS um ON rdbM.AppUserId=um.App_user_id
	LEFT JOIN BUSDTA.Reserve_DeviceEnvironmentRoute AS der
		ON rdbM.EnvironmentMasterId=der.EnvironmentMasterId AND rdbM.RouteID=der.RouteID AND rdbM.DeviceID=der.DeviceID AND rdbM.AppUserID=der.AppUserID AND ReservedOn>=DATEADD(MINUTE, @ExpMins, GETDATE())
END
GO
CREATE PROCEDURE [dbo].[uspValidateRegisteration]
(
	@DeviceId VARCHAR(50)
)
AS
BEGIN
	DECLARE @ExpMins INT;
	/*get reservation expire minutes*/
	SELECT @ExpMins = (ExpireMins*-1) FROM SystemDB_FPS4.dbo.tblReserveConfig 

	SELECT ISNULL(ReservationID,0) AS ReservationID, rdb.DeviceID, em.EnvName, rdb.RouteID, rdb.ActiveYN,ReservedOn
	FROM BUSDTA.RemoteDBID_Master AS rdb 
	LEFT JOIN BUSDTA.Reserve_DeviceEnvironmentRoute AS der ON rdb.DeviceID=der.DeviceID AND rdb.EnvironmentMasterID=der.EnvironmentMasterID AND rdb.RouteID=der.RouteID 
	LEFT JOIN BUSDTA.Environment_Master AS em ON em.EnvironmentMasterId=rdb.EnvironmentMasterId  
	WHERE rdb.DeviceId=@DeviceId AND ISNULL(ReservedOn, GETDATE())>=DATEADD(MINUTE, @ExpMins, GETDATE())
END
GO

ALTER PROCEDURE [dbo].[Register_DeviceEnvironment] 
(
	@DeviceId VARCHAR(30),
	@RouteID VARCHAR(30),
	@EnvironmentName VARCHAR(30),
	@AppUserName VARCHAR(30)
)
AS
BEGIN
DECLARE @UserEnvironmentMapId INT=0, @EnvironmentMasterId INT, @AppUserId INT;
SELECT @EnvironmentMasterId = EnvironmentMasterId FROM BUSDTA.Environment_Master WHERE EnvName=@EnvironmentName
SELECT @AppUserId = App_User_ID FROM BUSDTA.User_Master WHERE App_User=@AppUserName
SELECT @UserEnvironmentMapId = ISNULL(MAX(UserEnvironmentMapId), 0)+1 FROM BUSDTA.User_Environment_Map

	IF EXISTS(SELECT 1 FROM BUSDTA.User_Environment_Map WHERE EnvironmentMasterId=@EnvironmentMasterId)
	BEGIN
		UPDATE BUSDTA.RemoteDBID_Master SET ActiveYN='Y' FROM BUSDTA.RemoteDBID_Master AS A
		WHERE A.DeviceId=@DeviceId AND A.RouteID=@RouteID AND A.EnvironmentMasterId=@EnvironmentMasterId
		AND RemoteDBID = (SELECT MAX(RemoteDBID) FROM BUSDTA.RemoteDBID_Master AS B WHERE A.AppUserId=B.AppUserId AND A.DeviceID=B.DeviceID GROUP BY AppUserId, DeviceID)
	END
	
	IF EXISTS(SELECT 1 FROM BUSDTA.User_Environment_Map WHERE EnvironmentMasterId=@EnvironmentMasterId)
	BEGIN
		DELETE FROM BUSDTA.Reserve_DeviceEnvironmentRoute WHERE DeviceId=@DeviceId AND RouteID=@RouteID AND EnvironmentMasterId=@EnvironmentMasterId
	END
END

GO

USE MobileDataModel_FPS4
GO

CREATE TABLE [BUSDTA].[RouteStatus](
	[RouteStatusID] [int] NOT NULL,
	[RouteStatus] [varchar](100) NULL,
	[StatusDescription] [varchar](500) NULL,
	[last_modified] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[RouteStatusID] ASC
)
) ON [PRIMARY]

GO
ALTER TABLE [BUSDTA].[RouteStatus] ADD  CONSTRAINT [DF_BUSDTARotueStatus_lastModified]  DEFAULT (getdate()) FOR [last_modified]
GO

INSERT [BUSDTA].[RouteStatus] ([RouteStatusID], [RouteStatus], [StatusDescription]) VALUES (1, N'UNREGISTERED', N'NOT REGISTERED')
GO
INSERT [BUSDTA].[RouteStatus] ([RouteStatusID], [RouteStatus], [StatusDescription]) VALUES (2, N'PENDING REGISTRATION', N'RESERVED')
GO
INSERT [BUSDTA].[RouteStatus] ([RouteStatusID], [RouteStatus], [StatusDescription]) VALUES (3, N'REGISTERED', N'ON BOARDED')
GO

INSERT INTO BUSDTA.Route_Master(RouteMasterID, RouteName, RouteDescription, RouteAdressBookNumber, BranchNumber, DefaultUser, VehicleID, RouteStatusID)
select TOP 10 RouteMasterID, RouteName, RouteDescription, RouteAdressBookNumber, BranchNumber, DefaultUser, VehicleID,1 RouteStatusID from [HP-1\SQLEXPRESS2012].MobileDataModel_FPS4.BUSDTA.Route_Master

GO

INSERT INTO BUSDTA.Route_User_Map(App_user_id, Route_Id, Active)
select App_user_id, Route_Id, Active from [HP-1\SQLEXPRESS2012].MobileDataModel_FPS4.BUSDTA.Route_User_Map WHERE App_user_id<=10
GO
UPDATE BUSDTA.Route_User_Map SET App_user_id=1 WHERE Route_Id!='FBM057'
GO

CREATE PROCEDURE [dbo].[Register_DeviceRoute]
(
	@DeviceId VARCHAR(30),
	@RouteID VARCHAR(30)
)
AS
BEGIN
	IF EXISTS(SELECT 1 FROM BUSDTA.Route_Master WHERE RouteName=@RouteID)
	BEGIN
		UPDATE BUSDTA.Route_Master SET RouteStatusID=(SELECT RouteStatusID FROM busdta.RouteStatus WHERE RouteStatus='REGISTERED')
			WHERE RouteName=@RouteID
		UPDATE BUSDTA.Route_Device_Map SET Active=1 WHERE Device_Id=@DeviceId AND Route_Id=@RouteID
	END
END
GO

CREATE PROCEDURE [dbo].[Reserve_DeviceRoute]
(
	@DeviceId VARCHAR(30),
	@RouteID VARCHAR(30),
	@DBID INT
)
AS
BEGIN
	IF NOT EXISTS(SELECT 1 FROM BUSDTA.Route_Device_Map WHERE Route_Id=@RouteID AND Device_Id=@DeviceId)
	BEGIN
		INSERT INTO BUSDTA.Route_Device_Map (Route_Id, Device_Id, Active, Remote_Id)
		VALUES (@RouteID, @DeviceId, 0, CONCAT(@DeviceId, '_', @RouteID))
	END
	IF NOT EXISTS(SELECT 1 FROM ml_user WHERE name=CAST(@DBID AS VARCHAR))
	BEGIN
		INSERT INTO ml_user (name) VALUES (@DBID)
	END
	UPDATE BUSDTA.Route_Master SET RouteStatusID =(SELECT RouteStatusID FROM BUSDTA.RouteStatus WHERE RouteStatus='PENDING REGISTRATION')
	WHERE RouteName=@RouteID AND RouteStatusID =(SELECT RouteStatusID FROM BUSDTA.RouteStatus WHERE RouteStatus='UNREGISTERED')
END
GO


/*
=============================================
Author:			Sakaravarthi J
Create date:	28-Sep-2015
Description:	Logs Webservices' request and response
	Logs WebServices source from all sources
		1. Stats Services
		2. Onboarding Services
Note: this Stored Procedure calls SystemDB's [SaveWebServiceLog] Procedure and Logs Request and Response SystemDB
=============================================
*/
ALTER PROCEDURE [dbo].[SaveWebServiceLog]
(
	@DeviceID VARCHAR(100)='',
	@Request NVARCHAR(MAX),
	@Response NVARCHAR(MAX),
	@Environment VARCHAR(128)='Development',
	@ServiceName VARCHAR(100)='',
	@ServiceEndPoint VARCHAR(1000)='',
	@ServiceContract VARCHAR(1000)=''
)
AS
BEGIN
SET NOCOUNT ON
	DECLARE @DBName VARCHAR(128); 
	SET @DBName = (SELECT DB_NAME());

	EXEC [SystemDBFPS4].[dbo].[SaveWebServiceLog] 
			@DeviceID=@DeviceID, 
			@Request =@Request,
			@Response =@Response,
			@DBName = @DBName,
			@Environment = @Environment,
			@ServiceName = @ServiceName,
			@ServiceEndPoint = @ServiceEndPoint,
			@ServiceContract = @ServiceContract
SET NOCOUNT OFF
END
GO

SELECT @@SERVERNAME
