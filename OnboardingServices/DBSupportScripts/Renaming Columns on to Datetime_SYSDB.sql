/*
SystemDB
*/
select * from sys.tables where type='U'

select object_name(object_id), * from sys.columns
where object_id in(
	select object_id from sys.tables where type='U'
)
and system_type_id=167 and name in('CreatedBy', 'EditedBy')

select * from sys.types

--update tblWebService_ReleaseMaster SET CreatedBy=1, EditedBy=1
--update tblServiceResponses SET CreatedBy=1, EditedBy=1
--update tblReserveConfig SET CreatedBy=1, EditedBy=1
--GO

--ALTER TABLE tblWebService_ReleaseMaster ALTER COLUMN CreatedBy INT
--ALTER TABLE tblWebService_ReleaseMaster ALTER COLUMN EditedBy INT
--ALTER TABLE tblServiceResponses ALTER COLUMN CreatedBy INT
--ALTER TABLE tblServiceResponses ALTER COLUMN EditedBy INT
--ALTER TABLE tblReserveConfig ALTER COLUMN CreatedBy INT
--ALTER TABLE tblReserveConfig ALTER COLUMN EditedBy INT


select object_name(object_id), * from sys.columns
where object_id in(
	select object_id from sys.tables where type='U'
)
and name in('CreatedOn', 'EditedOn')
GO

SP_RENAME 'tblWebService_ReleaseMaster.CreatedOn', 'CreatedDateTime', 'COLUMN'
GO
SP_RENAME 'tblWebService_ReleaseMaster.EditedOn', 'EditedDateTime', 'COLUMN'
GO
SP_RENAME 'tblServiceResponses.CreatedOn', 'CreatedDateTime', 'COLUMN'
GO
SP_RENAME 'tblServiceResponses.EditedOn', 'EditedDateTime', 'COLUMN'
GO
SP_RENAME 'tblReserveConfig.CreatedOn', 'CreatedDateTime', 'COLUMN'
GO
SP_RENAME 'tblReserveConfig.EditedOn', 'EditedDateTime', 'COLUMN'
GO
SP_RENAME 'BUSDTA.RemoteDBID_Master.CreatedOn', 'CreatedDateTime', 'COLUMN'
GO
--SP_RENAME 'RemoteDBID_Master.EditedOn', 'EditedDateTime', 'COLUMN'
SP_RENAME 'BUSDTA.Reserve_DeviceEnvironmentRoute.ReservedOn', 'ReservedDateTime', 'COLUMN'


SELECT * FROM BUSDTA.RemoteDBID_Master
GO
SELECT * FROM BUSDTA.Reserve_DeviceEnvironmentRoute

SELECT OBJECT_NAME(id),* FROM sys.syscomments where text like'%CreatedOn%'
GO
SELECT OBJECT_NAME(id),* FROM sys.syscomments where text like'%EditedOn%'
GO
SELECT OBJECT_NAME(id),* FROM sys.syscomments where text like'%ReservedOn%'

GO

ALTER PROCEDURE [dbo].[uspCleanupReservation_SysDB]
AS
BEGIN
	DECLARE @ExpMins INT;
	/*get reservation expire minutes*/
	SELECT @ExpMins = (ExpireMins*-1) FROM tblReserveConfig 
	
	--Remove Entry from Reservation Table Reserve_DeviceEnvironmentRoute
	DELETE FROM BUSDTA.Reserve_DeviceEnvironmentRoute WHERE ReservedDateTime<DATEADD(MINUTE, @ExpMins, GETDATE())
	--DELETE FROM BUSDTA.RemoteDBID_Master WHERE CreatedDateTime<DATEADD(MINUTE, @ExpMins, GETDATE()) AND ActiveYN='N'
END
GO

ALTER TRIGGER [dbo].[tblWebService_ReleaseMaster_upd]
	ON [dbo].[tblWebService_ReleaseMaster] AFTER UPDATE
AS
	/* Update the column EditedDateTime in modified row. */
	UPDATE dbo.tblWebService_ReleaseMaster
	SET EditedDateTime = GETDATE()
	FROM inserted
	WHERE  dbo.tblWebService_ReleaseMaster.ReleaseMasterID = inserted.ReleaseMasterID;
GO

ALTER PROCEDURE [dbo].[Reserve_DeviceEnvironmentRoute]
(
	@DeviceId VARCHAR(30),
	@EnvironmentName VARCHAR(30),
	@RouteID VARCHAR(30),
	@AppUserName VARCHAR(30)
)
AS
BEGIN
DECLARE @EnvironmentMasterId INT, @AppUserId INT, @rdb_script_location VARCHAR(MAX), @RemoteDBID INT;
SELECT @EnvironmentMasterId = EnvironmentMasterId, @rdb_script_location=RDBScriptLocation FROM BUSDTA.Environment_Master WHERE EnvName=@EnvironmentName
SELECT @AppUserId = App_User_ID FROM BUSDTA.User_Master WHERE App_User=@AppUserName
SELECT @RemoteDBID = ISNULL(MAX(RemoteDBID),1000) FROM BUSDTA.RemoteDBID_Master

	IF NOT EXISTS(SELECT 1 FROM BUSDTA.Reserve_DeviceEnvironmentRoute WHERE DeviceId=@DeviceId AND EnvironmentMasterId=@EnvironmentMasterId AND RouteID=@RouteID)
	BEGIN
		INSERT INTO BUSDTA.Reserve_DeviceEnvironmentRoute(DeviceId, EnvironmentMasterId, RouteID, AppUserId)
		SELECT @DeviceId, @EnvironmentMasterId, @RouteID, @AppUserId
	END
	ELSE
	BEGIN
		UPDATE BUSDTA.Reserve_DeviceEnvironmentRoute SET ReservedDateTime=GETDATE() 
		WHERE DeviceId=@DeviceId AND EnvironmentMasterId=@EnvironmentMasterId AND RouteID=@RouteID
	END

	--IF NOT EXISTS(SELECT 1 FROM BUSDTA.RemoteDBID_Master WHERE DeviceId=@DeviceId AND EnvironmentMasterId=@EnvironmentMasterId AND RouteID=@RouteID)
	BEGIN
		SET @RemoteDBID=@RemoteDBID+1
		INSERT INTO BUSDTA.RemoteDBID_Master (RemoteDBID, AppUserId, DeviceID, RouteID, EnvironmentMasterID)
		VALUES (@RemoteDBID, @AppUserId, @DeviceId, @RouteID,@EnvironmentMasterId)
	END

	SELECT @rdb_script_location RDBScriptLocation, @RemoteDBID RemoteDBID
END
GO

ALTER PROCEDURE [dbo].[uspCheckRouteAvailabilityForReserve]
AS
BEGIN
	DECLARE @ExpMins INT;
	/*get reservation expire minutes*/
	SELECT @ExpMins = (ExpireMins*-1) FROM tblReserveConfig 

	SELECT rdbM.DeviceID, um.App_User, EnvName, rdbM.RouteID, rdbM.ActiveYN, ReservedDateTime
	FROM BUSDTA.RemoteDBID_Master AS rdbM
	INNER JOIN BUSDTA.Environment_Master AS em ON rdbM.EnvironmentMasterId=em.EnvironmentMasterId 
	INNER JOIN BUSDTA.User_Master AS um ON rdbM.AppUserId=um.App_user_id
	LEFT JOIN BUSDTA.Reserve_DeviceEnvironmentRoute AS der
		ON rdbM.EnvironmentMasterId=der.EnvironmentMasterId AND rdbM.RouteID=der.RouteID AND rdbM.DeviceID=der.DeviceID AND rdbM.AppUserID=der.AppUserID AND ReservedDateTime>=DATEADD(MINUTE, @ExpMins, GETDATE())
END
GO

ALTER PROCEDURE [dbo].[uspCleanupReservation_SysDB]
AS
BEGIN
	DECLARE @ExpMins INT;
	/*get reservation expire minutes*/
	SELECT @ExpMins = (ExpireMins*-1) FROM tblReserveConfig 
	
	--Remove Entry from Reservation Table Reserve_DeviceEnvironmentRoute
	DELETE FROM BUSDTA.Reserve_DeviceEnvironmentRoute WHERE ReservedDateTime<DATEADD(MINUTE, @ExpMins, GETDATE())
	--DELETE FROM BUSDTA.RemoteDBID_Master WHERE CreatedDateTime<DATEADD(MINUTE, @ExpMins, GETDATE()) AND ActiveYN='N'
END
GO

ALTER PROCEDURE uspValidateRegisteration
(
	@DeviceId VARCHAR(50)
)
AS
BEGIN
	DECLARE @ExpMins INT;
	/*get reservation expire minutes*/
	SELECT @ExpMins = (ExpireMins*-1) FROM EnvironmentDB.dbo.tblReserveConfig 

	SELECT ISNULL(ReservationID,0) AS ReservationID, rdb.DeviceID, em.EnvName, rdb.RouteID, rdb.ActiveYN,ReservedDateTime
	FROM BUSDTA.RemoteDBID_Master AS rdb 
	LEFT JOIN BUSDTA.Reserve_DeviceEnvironmentRoute AS der ON rdb.DeviceID=der.DeviceID AND rdb.EnvironmentMasterID=der.EnvironmentMasterID AND rdb.RouteID=der.RouteID 
	LEFT JOIN BUSDTA.Environment_Master AS em ON em.EnvironmentMasterId=rdb.EnvironmentMasterId  
	WHERE rdb.DeviceId=@DeviceId AND ISNULL(ReservedDateTime, GETDATE())>=DATEADD(MINUTE, @ExpMins, GETDATE())
END
GO

SELECT OBJECT_NAME(id),* FROM sys.syscomments where text like'%CreatedBy%'
GO
SELECT OBJECT_NAME(id),* FROM sys.syscomments where text like'%EditedBy%'
