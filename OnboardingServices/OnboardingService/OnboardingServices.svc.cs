﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using OnboardModels;
using DBModels;
using LogLib;
using System.IO;
using System.Net;

namespace OnboardingService
{
    /// <summary>
    /// Get Environment Route List
    /// </summary>

    public class OnboardingServices : IOnboardingServices
    {
        string _environment = "", _endpoint="", _deviceID=""; 

        public OnboardingServices()
        {
            _environment = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["Environment"]);
            _endpoint = Convert.ToString(OperationContext.Current.EndpointDispatcher.EndpointAddress);
            _deviceID = DeviceManager.Device.GenerateDeviceID();
        }

        [WebInvoke(Method = "GET")]
        public UserInfoResponse UserInfo(UserInfoRequest UserReq)
        {
            UserInfoResponse objResponse = new UserInfoResponse();
            try
            {
                UserInfoBL objUserInfoBL = new UserInfoBL();
                objResponse = objUserInfoBL.GetUserInformation(UserReq);

                string _contract = Convert.ToString(new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name);
                LogDetails objLogDetails = new LogDetails(_deviceID, UserReq, objResponse, _environment, this.GetType().Name, _endpoint, _contract);
                //bool resp = new LogBL().LogResponse(objLogDetails);

                if (objResponse==null)
                {
                    UserInfoException objUserInfoException = new UserInfoException();
                    objUserInfoException.RequestValues = UserReq;
                    throw new FaultException<UserInfoException>(objUserInfoException, new FaultReason("User Not Exists"), new FaultCode("User Not Exists"));
                }
                return objResponse;
            }
            catch (Exception ex)
            {
                UserInfoException objUserInfoException = new UserInfoException();
                objUserInfoException.RequestValues = UserReq;
                throw new FaultException<UserInfoException>(objUserInfoException, new FaultReason("User Not Exists"), new FaultCode("User Not Exists"));
            }
        }

        [WebInvoke(Method="GET")]
        public AuthorizeResponse Authorize(AuthorizeRequest AuthReq)
        {
            AuthorizeResponse objResponse = new AuthorizeResponse();
            try
            {
                AuthorizeBL objAuthorizeBL = new AuthorizeBL();
                objResponse = objAuthorizeBL.GetEnvironmentRoutes(AuthReq);

                string _contract = Convert.ToString(new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name);
                LogDetails objLogDetails = new LogDetails(AuthReq.DeviceID, AuthReq, objResponse, _environment, this.GetType().Name, _endpoint, _contract);
                //bool resp = new LogBL().LogResponse(objLogDetails);

                if (Convert.ToInt32(objResponse.MessageCode) > 101)
                {
                    AuthorizeException objAuthorizeException = new AuthorizeException();
                    objAuthorizeException.RequestValues = AuthReq;
                    throw new FaultException<AuthorizeException>(objAuthorizeException, new FaultReason(objResponse.Message), new FaultCode(objResponse.MessageCode));
                }
                return objResponse;
            }
            catch (Exception ex)
            {
                AuthorizeException objAuthorizeException = new AuthorizeException();
                objAuthorizeException.RequestValues = AuthReq;
                throw new FaultException<AuthorizeException>(objAuthorizeException, new FaultReason(objResponse.Message), new FaultCode(objResponse.MessageCode));
                
            }
        }

        [WebInvoke(Method = "POST")]
        public ReserveResponse Reserve(ReserveRequest RsrvReq)
        {
            ReserveResponse objResponse = new ReserveResponse();
            try
            {
                ReserveBL objReserveBL = new ReserveBL();
                objResponse = objReserveBL.ReserveEnvRouteForUser(RsrvReq);

                string _contract = Convert.ToString(new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name);
                LogDetails objLogDetails = new LogDetails(RsrvReq.DeviceID, RsrvReq, objResponse, _environment, this.GetType().Name, _endpoint, _contract);
                //bool resp = new LogBL().LogResponse(objLogDetails);

                if (Convert.ToInt32(objResponse.MessageCode) > 201)
                {
                    ReserveException objReserveException = new ReserveException();
                    objReserveException.RequestValues = RsrvReq;
                    throw new FaultException<ReserveException>(objReserveException, new FaultReason(objResponse.Message), new FaultCode(objResponse.MessageCode));
                }
                return objResponse;
            }
            catch (Exception ex)
            {
                ReserveException objReserveException = new ReserveException();
                objReserveException.RequestValues = RsrvReq;
                throw new FaultException<ReserveException>(objReserveException, new FaultReason(objResponse.Message), new FaultCode(objResponse.MessageCode));
            }
        }

        [WebInvoke(Method = "POST")]
        public RegisterResponse Register(RegisterRequest RegiReq)
        {
            RegisterResponse objResponse = new RegisterResponse();
            try
            {
                RegisterBL objRegisterBL = new RegisterBL();
                objResponse = objRegisterBL.RegisterEnvRouteForUser(RegiReq);

                string _contract = Convert.ToString(new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name);
                LogDetails objLogDetails = new LogDetails(RegiReq.DeviceID, RegiReq, objResponse, _environment, this.GetType().Name, _endpoint, _contract);
                //bool resp = new LogBL().LogResponse(objLogDetails);
                objResponse.RegisterationParms = RegiReq;

                if (Convert.ToInt32(objResponse.MessageCode) > 301)
                {
                    RegisterException objRegisterException = new RegisterException();
                    objRegisterException.RequestValues = RegiReq;
                    throw new FaultException<RegisterException>(objRegisterException, new FaultReason(objResponse.Message), new FaultCode(objResponse.MessageCode));
                }
                return objResponse;
            }
            catch (Exception ex)
            {
                RegisterException objRegisterException = new RegisterException();
                objRegisterException.RequestValues = RegiReq;
                throw new FaultException<RegisterException>(objRegisterException, new FaultReason(objResponse.Message), new FaultCode(objResponse.MessageCode));
            }
        }

        [WebInvoke(Method = "GET")]
        public List<Application> AppInfo(Application objAppInfo)
        {
            List<Application> objResponse = new List<Application>();
            try
            {
                AppInfoBL objAppInfoBL = new AppInfoBL();
                string AppName =string.IsNullOrWhiteSpace(objAppInfo.AppName) ? "" : objAppInfo.AppName == "?" ? "" : objAppInfo.AppName;
                string EnvName = string.IsNullOrWhiteSpace(objAppInfo.EnvironmentName) ? "" : objAppInfo.EnvironmentName == "?" ? "" : objAppInfo.EnvironmentName;
                objResponse = objAppInfoBL.GetAppInfo(AppName, EnvName);

                string _contract = Convert.ToString(new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name);
                LogDetails objLogDetails = new LogDetails(_deviceID, objAppInfo, objResponse, _environment, this.GetType().Name, _endpoint, _contract);
                //bool resp = new LogBL().LogResponse(objLogDetails);

                return objResponse;
            }
            catch (Exception ex)
            {
                throw new FaultException(new FaultReason("App Not Exists"), new FaultCode("App Not Exists"));
            }
        }

        #region Download File

        public Stream RetrieveFile(string path)
        {
            if (WebOperationContext.Current == null) throw new Exception("WebOperationContext not set");

            // As the current service is being used by a windows client, there is no browser interactivity.
            // In case you are using the code Web, please use the appropriate content type.
            var fileName = Path.GetFileName(path);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/zip";
            WebOperationContext.Current.OutgoingResponse.Headers.Add("content-disposition", "attachment; filename=" + fileName);

            return File.OpenRead(path);
        }  

        #endregion

        [WebInvoke(Method = "GET")]
        public DBVersionResponse FetchDBVersion(DBVersionRequest DBReq)
        {
            DBVersionResponse objResponse = new DBVersionResponse();
            try
            {
                DBVersionInfoBL objDBVersionBL = new DBVersionInfoBL();
                objResponse = objDBVersionBL.GetDBVersionInfo(DBReq);

                string _contract = Convert.ToString(new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name);
                LogDetails objLogDetails = new LogDetails(DBReq.DeviceID, DBReq, objResponse, _environment, this.GetType().Name, _endpoint, _contract);
                //bool resp = new LogBL().LogResponse(objLogDetails);

                return objResponse;
            }
            catch (Exception ex)
            {
                DBVersionException objRegisterException = new DBVersionException();
                objRegisterException.RequestValues = DBReq;
                throw new FaultException<DBVersionException>(objRegisterException, new FaultReason(objResponse.Message), new FaultCode(objResponse.MessageCode));
            }
        }
    }
}
