﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using OnboardModels;
using System.IO;

namespace OnboardingService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IOnboardingServices
    {
        [OperationContract]
        [FaultContract(typeof(UserInfoException))]
        [WebGet(UriTemplate = "/UserInfo?v={AuthReq}"/*, BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Xml*/)]
        UserInfoResponse UserInfo(UserInfoRequest AuthReq);

        [OperationContract]
        [FaultContract(typeof(AuthorizeException))]
        AuthorizeResponse Authorize(AuthorizeRequest AuthReq);

        [OperationContract]
        [FaultContract(typeof(ReserveException))]
        ReserveResponse Reserve(ReserveRequest RsrvReq);
        
        [OperationContract]
        [FaultContract(typeof(RegisterException))]
        RegisterResponse Register(RegisterRequest RegiReq);

        [OperationContract]
        List<Application> AppInfo(Application AppReq);

        // Added by karthic on 23.12.2015
        [OperationContract]                
        Stream RetrieveFile(string path);

        [OperationContract]
        [FaultContract(typeof(DBVersionException))]
        DBVersionResponse FetchDBVersion(DBVersionRequest DBReq);

    }
}
