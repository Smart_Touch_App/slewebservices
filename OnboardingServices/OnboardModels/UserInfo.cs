﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace OnboardModels
{
    [DataContract]
    public class UserInfoRequest
    {
        [DataMember(Name = "DomainUser", Order = 0)]
        public string DomainUser { get; set; }
        [DataMember(Name = "DomainName", Order = 1)]
        public string DomainName { get; set; }
        [DataMember(Name = "AppPassword", Order = 2)]
        public string AppPassword { get; set; }
    }
    [DataContract]
    public class UserInfoResponse
    {
        [DataMember(Name = "App_user_id", Order = 0)]
        public int App_user_id { get; set; }
        [DataMember(Name = "App_User", Order = 1)]
        public string App_User { get; set; }
        [DataMember(Name = "Name", Order = 2)]
        public string Name { get; set; }
        [DataMember(Name = "DomainUser", Order = 3)]
        public string DomainUser { get; set; }
        [DataMember(Name = "AppPassword", Order = 4)]
        public string AppPassword { get; set; }
        [DataMember(Name = "active", Order = 5)]
        public bool active { get; set; }
    }
    [DataContract]
    public class UserInfoException
    {
        [DataMember(Name = "RequestValues", Order = 1)]
        public UserInfoRequest RequestValues { get; set; }
    }
}
