﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace OnboardModels
{
    [DataContract]
    public class RegisterRequest
    {
        [DataMember(Name = "DeviceID", Order = 0)]
        public string DeviceID { get; set; }
        [DataMember(Name = "EnvironmentName", Order = 1)]
        public string EnvironmentName { get; set; }
        [DataMember(Name = "RouteID", Order = 2)]
        public string RouteID { get; set; }
        [DataMember(Name = "UserName", Order = 3)]
        public string UserName { get; set; }
        [DataMember(Name = "AuthenticatedToken", Order = 4)]
        public string AuthenticatedToken { get; set; }
    }

    [DataContract]
    public class RegisterResponse
    {
        [DataMember(Name = "MessageCode", Order = 0)]
        public string MessageCode { get; set; }

        [DataMember(Name = "Message", Order = 1)]
        public string Message { get; set; }
        
        [DataMember(Name = "RegisterationParms", Order = 2)]
        public RegisterRequest RegisterationParms { get; set; }
    }

    [DataContract]
    public class RegisterException
    {
        [DataMember(Name = "RequestValues", Order = 1)]
        public RegisterRequest RequestValues { get; set; }
    }
}
