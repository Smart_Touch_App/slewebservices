﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace OnboardModels
{
    [DataContract]
    public class Environment
    {

        [DataMember(Name = "AppUserID", Order = 0)]
        public string AppUserID { get; set; }
        [DataMember(Name = "AppUserName", Order = 1)]
        public string AppUserName { get; set; }

        [DataMember(Name = "EnvironmentId", Order = 2)]
        public string EnvironmentId { get; set; }
        [DataMember(Name = "EnvironmentName", Order = 3)]
        public string EnvironmentName { get; set; }
        [DataMember(Name = "EnvironmentDescription", Order = 4)]
        public string EnvironmentDescription { get; set; }

        [DataMember(Name = "RouteList", Order = 5)]
        public List<Route> RouteList { get; set; }

    }

    [DataContract]
    public class Route
    {
        [DataMember(Name = "RouteId", Order = 0)]
        public string RouteId { get; set; }
        [DataMember(Name = "RouteDescription", Order = 1)]
        public string RouteDescription { get; set; }
        [DataMember(Name = "DefaultUser", Order = 2)]
        public string DefaultUser { get; set; }
        [DataMember(Name = "RouteStatus", Order = 3)]
        public string RouteStatus { get; set; }
        [DataMember(Name = "RouteStatusDescription", Order = 4)]
        public string RouteStatusDescription { get; set; }
    }
}
