﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace OnboardModels
{
    [DataContract]
    public class ReserveRequest
    {
        [DataMember(Name = "DeviceID", Order = 0)]
        public string DeviceID { get; set; }
        [DataMember(Name = "EnvironmentName", Order = 1)]
        public string EnvironmentName { get; set; }
        [DataMember(Name = "RouteID", Order = 2)]
        public string RouteID { get; set; }
        [DataMember(Name = "UserName", Order = 3)]
        public string UserName { get; set; }
        [DataMember(Name = "AuthenticatedToken", Order = 4)]
        public string AuthenticatedToken { get; set; }
    }

    [DataContract]
    public class ReserveResponse
    {
        [DataMember(Name = "MessageCode", Order = 0)]
        public string MessageCode { get; set; }

        [DataMember(Name = "Message", Order = 1)]
        public string Message { get; set; }

        [DataMember(Name = "DeviceID", Order = 2)]
        public string DeviceID { get; set; }

        [DataMember(Name = "AppUser", Order = 3)]
        public string AppUser { get; set; }

        [DataMember(Name = "EnvironmentName", Order = 4)]
        public string EnvironmentName { get; set; }

        [DataMember(Name = "RouteID", Order = 5)]
        public string RouteID { get; set; }

        [DataMember(Name = "RemoteDbID", Order = 6)]
        public string RemoteDbID { get; set; }

        [DataMember(Name = "SetupURL", Order = 7)]
        public List<Application> SetupURL { get; set; }
    }

    [DataContract]
    public class Application
    {

        [DataMember(Name = "AppType", Order = 0)]
        public string AppType { get; set; }

        [DataMember(Name = "AppName", Order = 1)]
        public string AppName { get; set; }

        [DataMember(Name = "ReleasedDate", Order = 2)]
        public DateTime? ReleasedDate { get; set; }

        [DataMember(Name = "AppVersion", Order = 3)]
        public string AppVersion { get; set; }

        [DataMember(Name = "AppLocation", Order = 4)]
        public string AppLocation { get; set; }

        [DataMember(Name = "IsMandatory", Order = 5)]
        public bool IsMandatory { get; set; }

        [DataMember(Name = "EnvironmentName", Order = 6)]
        public string EnvironmentName { get; set; }
    }
    
    [DataContract]
    public class ReserveException
    {
        [DataMember(Name = "RequestValues", Order = 1)]
        public ReserveRequest RequestValues { get; set; }
    }

}
