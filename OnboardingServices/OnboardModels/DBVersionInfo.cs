﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace OnboardModels
{
    [DataContract]
    public class DBVersionRequest
    {
        [DataMember(Name = "DeviceID", Order = 0)]
        public string DeviceID { get; set; }
        [DataMember(Name = "UserName", Order = 1)]
        public string UserName { get; set; }
        [DataMember(Name = "RemoteCDBVersion", Order = 2)]
        public string RemoteCDBVersion { get; set; }
        [DataMember(Name = "RemoteSysDBVersion", Order = 3)]
        public string RemoteSysDBVersion { get; set; }
    }

    [DataContract]
    public class DBVersionResponse
    {
        [DataMember(Name = "ReleaseName", Order = 0)]
        public string ReleaseName { get; set; }
        [DataMember(Name = "SysDBVersion", Order = 1)]
        public string SysDBVersion { get; set; }
        [DataMember(Name = "CDBVersion", Order = 2)]
        public string CDBVersion { get; set; }

        [DataMember(Name = "MessageCode", Order = 3)]
        public string MessageCode { get; set; }
        [DataMember(Name = "Message", Order = 4)]
        public string Message { get; set; }
    }
    [DataContract]
    public class DBVersionException
    {
        [DataMember(Name = "RequestValues", Order = 1)]
        public DBVersionRequest RequestValues { get; set; }
    }
}
