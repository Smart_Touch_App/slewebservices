﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace OnboardModels
{
    [DataContract]
    public class AuthorizeRequest
    {
        [DataMember(Name = "DeviceID", Order = 0)]
        public string DeviceID { get; set; }
        [DataMember(Name = "UserName", Order = 1)]
        public string UserName { get; set; }
        [DataMember(Name = "AuthenticatedToken", Order = 2)]
        public string AuthenticatedToken { get; set; }
    }

    [DataContract]
    public class AuthorizeResponse
    {
        [DataMember(Name = "MessageCode", Order = 0)]
        public string MessageCode { get; set; }

        [DataMember(Name = "Message", Order = 1)]
        public string Message { get; set; }

        [DataMember(Name = "EnvironmentList", Order = 2)]
        public List<Environment> EnvironmentList { get; set; }
    }

    [DataContract]
    public class AuthorizeException
    {
        [DataMember(Name = "RequestValues", Order = 1)]
        public AuthorizeRequest RequestValues { get; set; }
    }
}
